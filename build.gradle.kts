buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.20")
    }
}

allprojects {
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs += listOf("-Xcontext-receivers")
        }
    }
}

plugins {
    kotlin("jvm") version "1.7.20"
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.7.20")
    implementation("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.3.5")
}

val MAIN_CLASS = "knife.MainKt"
val BINARY_NAME = "knife"
application.mainClass.set(MAIN_CLASS)
val jar by tasks.getting(Jar::class) {
    configurations["compileClasspath"].forEach { from(zipTree(it.absoluteFile)) }
    duplicatesStrategy = org.gradle.api.file.DuplicatesStrategy.WARN

    manifest.attributes["Main-Class"] = MAIN_CLASS

    destinationDirectory.set(file("build/outputs"))
    archiveBaseName.set(BINARY_NAME)
}

val shebangify = tasks.create("shebangify") {
    dependsOn(jar)
    val out = jar.destinationDirectory.get().asFile
    inputs.file(out.resolve("$BINARY_NAME.jar"))
    outputs.file(out.resolve(BINARY_NAME))
    doFirst {
        val shebang = "#!/usr/bin/env -S java -jar\n"
        out.resolve(BINARY_NAME).outputStream().use { output ->
            output.write(shebang.toByteArray(Charsets.UTF_8))
            out.resolve("$BINARY_NAME.jar").inputStream().copyTo(output)
        }
    }
}

val makeExecutable = tasks.create("makeExecutable") {
    dependsOn(shebangify)
    doFirst {
        val out = jar.destinationDirectory.get().asFile
        val path = file(out.resolve(BINARY_NAME)).absolutePath
        val chmodProcess = ProcessBuilder("chmod", "+x", path).run {
            inheritIO()
            start()
        }
        chmodProcess.waitFor().let { if (it != 0) error("chmod returned with non-zero exit code: $it") }
    }
}

tasks.named("build") {
    dependsOn(jar, shebangify, makeExecutable)
}
