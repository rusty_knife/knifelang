Just a language to play around with a bit. Statically typed, procedural, compiled to llvm at some point hopefully...


## Documentation ##
Who?


## TODO ##
- [ ] Fix the weird bug where sometimes type inference doesn't work for locals?
- [ ] Unary operators (aka calling functions with operator names without brackets ig?)
- [ ] Infix call priority
- [ ] Short-circuit evaluation
- [ ] Check that all paths actually return
- [ ] Flat IR
- [ ] LLVM
- [ ] Inheritance/traits/whatever
- [ ] Lambdas and function types
