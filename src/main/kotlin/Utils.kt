package knife


fun String.indent(indent: String) = replace("\n", "\n$indent")

inline fun <T> Iterable<T>.equal(other: Iterable<T>, equal: (first: T, second: T) -> Boolean): Boolean {
    val firstIterator = this.iterator()
    val secondIterator = other.iterator()

    while (firstIterator.hasNext() && secondIterator.hasNext()) {
        if (!equal(firstIterator.next(), secondIterator.next())) return false
    }

    return !firstIterator.hasNext() && !secondIterator.hasNext()
}

inline fun <K, V> Map<K, V>.equal(other: Map<K, V>, equal: (first: V, second: V) -> Boolean): Boolean {
    val otherKeys = other.keys.toMutableSet()
    keys.forEach { key ->
        if (!otherKeys.remove(key)) return false
        @Suppress("UNCHECKED_CAST")
        if (!equal(get(key) as V, other.get(key) as V)) return false
    }
    return otherKeys.isEmpty()
}


var enableLogging = false
fun logln(message: Any?) { if (enableLogging) println(message) }
fun log(message: Any?) { if (enableLogging) print(message) }


inline fun <reified T> Any?.takeIfIs(): T? = this as? T

inline fun <reified T, R> Any?.letIfIs(block: (T) -> R): R? = takeIfIs<T>()?.let(block)
inline fun <reified T> Any?.letIfIs(block: (T) -> Unit): Unit { takeIfIs<T>()?.let(block) }
