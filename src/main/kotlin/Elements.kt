package knife


data class Function(
    val location: Location,
    val rootScope: Scope,
    val name: String,
    val signature: Signature,
    val body: Body,
    val tags: List<Expression.Tag>,
) : Scope.Element {
    init {
        // I don't like this but eh
        signature.owner = this
        body.owner = this
    }

    override fun toString(): String = buildString {
        append("Function(")
        append("`$name` defined at $location, ")
        append("signature = $signature")
        append(")")
    }

    data class Signature(
        val generics: List<DeclaredType.Generic>,
        val arguments: List<Argument>,
        val declaredReturnType: ActualType?,
    ) {
        lateinit var owner: Function
        val returnType: ActualType get() = declaredReturnType ?: owner.body.returnType
    }


    data class Argument(
        override val name: String,
        override val id: Long,
        val declaredType: ActualType?,
        override val mutable: Boolean,
        val default: Function?,
    ) : Variable, Variable.LocalLike {
        override val type: ActualType get() = declaredType ?: default?.signature?.returnType ?: error("Argument doesn't have a type or default value!")
    }


    data class Body(
        val scope: Scope,
        val source: List<Expression>?,
        val declaredReturnType: ActualType?,
        val isConstructor: Boolean = false,
    ) : Scope.Compilable {
        lateinit var owner: Function

        val returnType get() = declaredReturnType ?: compiled.returnType

        private var _compiled: Compiled? = null
        val compiled: Compiled get() {
            compile()
            return _compiled!!
        }
        override fun compile() {
            if (isConstructor) error("Attempt to compile a constructor")
            if (_compiled != null) return
            _compiled = when {
                owner.tags.any { it.name == "intrinsic" } -> owner.compileIntrinsic()
                else -> owner.compileActual()
            }
        }

        sealed interface Compiled {
            val returnType: ActualType

            data class Intrinsic(val implementation: Function.Intrinsic) : Compiled {
                override val returnType get() = implementation.signature.returnType
            }

            data class Actual(
                val instructions: List<Ir.Operation>,
                override val returnType: ActualType,
            ) : Compiled
        }
    }


    data class Intrinsic(
        val name: String,
        val signature: Signature,
    )
}

context(Scope) fun Expression.Function.include(): Function {
    val rootScope = subscope("fun `$name` (root)", keepLocals = false)

    // Generic type definitions
    val generics = mutableListOf<DeclaredType.Generic>()
    with(rootScope) {
        genericTypes.forEach { generic ->
            val type = DeclaredType.Generic(generic.name, bounds = generic.bounds?.resolve() ?: ActualType.Any)
            defineType(generic.name, type)
            generics.add(type)
        }
    }

    val arguments = mutableListOf<Function.Argument>()
    val argumentScope = rootScope

    logln("Including function $name, receiver type is $receiverType")
    if (receiverType != null) {
        val declaredType = with(argumentScope) { receiverType.resolve() }
        val result = Function.Argument(name = "\$this", id = argumentScope.nextLocalId, declaredType, mutable = false, default = null)
        logln("Generating \$this argument from receiver for $name")
        argumentScope.define(result.name, result)
        arguments.add(result)
    }

    this.arguments.forEach { argument ->
        val declaredType = with(argumentScope) { argument.type?.resolve() }
        val id = argumentScope.nextLocalId

        val default = argument.default?.let { default ->
            val scope = subscope("default of `${argument.name}` in `$name`")
            val name = "\$default\$${name}\$$id"

            @Suppress("NAME_SHADOWING")
            val arguments = arguments.map { original ->
                Function.Argument(
                    original.name,
                    id = scope.nextLocalId,
                    declaredType = original.declaredType ?: ActualType.Deferred { original.type },
                    mutable = false,
                    default = null,
                ).also { scope.define(it.name, it) }
            }

            val signature = Function.Signature(generics, arguments, declaredType)

            val bodyScope = scope.subscope("fun `$name` (body)")
            val body = Function.Body(
                scope = bodyScope,
                source = listOf(Expression.Return(default.location, default)),
                declaredReturnType = declaredType,
            )
            bodyScope.compilables.add(body)
            define(name, Function(argument.location, scope, name, signature, body, tags = emptyList()))
        }

        val result = Function.Argument(argument.name, id, declaredType, argument.mutable, default)
        argumentScope.define(argument.name, result)
        arguments.add(result)
    }

    val bodyScope = argumentScope.subscope("fun `$name` (body)")
    val declaredReturnType = with(bodyScope) { returnType?.resolve() }

    val body = Function.Body(
        scope = bodyScope,
        source = body,
        declaredReturnType,
    )
    bodyScope.compilables.add(body)

    val signature = Function.Signature(generics, arguments, declaredReturnType)

    return define(name, Function(location, rootScope, name, signature, body, tags))
}


sealed interface Variable : Scope.Element {
    data class Global(
        val name: String,
        val id: Long,
        val declaredType: ActualType?,
        val initializer: Initializer,
    ) : Variable {
        init { initializer.owner = this }

        val type: ActualType get() = declaredType ?: initializer.instruction.returnType

        data class Initializer(
            val scope: Scope,
            val source: Expression,
            val expectedType: ActualType?,
        ) : Scope.Compilable {
            lateinit var owner: Variable.Global

            private var _instruction: Ir.Operation? = null
            val instruction: Ir.Operation get() {
                compile()
                return _instruction!!
            }
            override fun compile() {
                if (_instruction != null) return
                _instruction = with(scope) { source.compileValueExpression(expectedType) }
            }
        }
    }


    sealed interface LocalLike : Variable {
        val name: String
        val type: ActualType
        val mutable: Boolean
        val id: Long
    }
    data class Local(
        override val name: String,
        override val id: Long,
        override val mutable: Boolean,
        override val type: ActualType,
    ) : Variable, LocalLike
}

context(Scope) fun Expression.Variable.include(): Variable.Global {
    val declaredType = type?.resolve()
    if (initializer == null) location.error("Globals need to have an initializer")
    if (mutable) location.error("Mutable globals are not supported yet")

    val initializerScope = subscope("val `$name` (initializer)", keepLocals = false)
    val initializer = Variable.Global.Initializer(initializerScope, initializer, declaredType)
    compilables.add(initializer)

    return define(name, Variable.Global(name, nextGlobalId, declaredType, initializer))
}
