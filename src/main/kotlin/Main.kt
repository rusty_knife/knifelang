package knife

import java.nio.file.Path
import kotlin.io.path.*


fun main(args: Array<String>) {
    val sourceFiles = mutableListOf<Path>()

    args.forEach { arg ->
        when (arg) {
            "-v", "--verbose" -> enableLogging = true
            else -> sourceFiles.add(Path(arg))
        }
    }

    val ast = mutableListOf<Expression>()
    sourceFiles.sourceFileSequence().forEach { file ->
        val source = file.run { Source(name, readText()) }
        val tokens = source.tokenize()
        ast += parse(tokens)
    }

    val scope = Scope()
    scope.stringLiteralType = BasicIntrinsics.Array.toActualOrNull(ActualType.Byte)!!
    val intrinsics = BasicIntrinsics(scope)

    scope.include(ast)
    scope.compile()

    val astInterpreter = AstInterpreter(intrinsicMatcher)
    scope.globals.forEach { astInterpreter.initGlobal(it) }
    val main = scope.resolveOrNull("main")?.filterIsInstance<Function>()?.singleOrNull() ?: error("No or ambiguous main function")
    logln("\n-- RUN --\n")
    astInterpreter.run(main)
}


fun List<Path>.sourceFileSequence(): Sequence<Path> = sequence {
    forEach { when {
        it.isDirectory() -> yieldAll(it.listDirectoryEntries().sourceFileSequence())
        it.extension == "knf" -> yield(it)
        else -> logln("Ignoring file $this because it's not a knife file")
    } }
}
