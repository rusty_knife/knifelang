package knife

import java.util.Deque
import java.util.LinkedList


class Scope private constructor(
    val path: String,
    val localParent: Scope?, // For example if this is a scope of a loop, this would be the outside scope in the same function
    val globalParent: Scope?, // For example in any scope within a function, this would be the scope just outside the function
) {
    var enableLogging = false
    private fun logln(message: Any?) { if (enableLogging) knife.logln(message) }

    constructor() : this(path = "<ROOT>", localParent = null, globalParent = null)

    fun subscope(name: String, keepLocals: Boolean = true) =
        if (keepLocals) Scope(path = "${this.path} -> $name", localParent = this, globalParent = globalParent)
        else Scope(path = "${this.path} -> $name", localParent = null, globalParent = this)


    val compilables: MutableList<Compilable> = localParent?.compilables ?: globalParent?.compilables ?: mutableListOf()

    val globals = mutableListOf<Variable.Global>()

    private val functions = mutableMapOf<String, MutableList<Function>>()
    private val variables = mutableMapOf<String, Variable>()

    fun resolveOrNull(name: String): List<Element>? {
        logln("Resolving $name")
        val result = buildList { resolve(name, allowVariables = true) }
        return if (result.isEmpty()) null else result
    }
    context(MutableList<Element>) private fun resolve(name: String, allowVariables: Boolean) {
        functions[name]?.let { addAll(it.asReversed()) }
        logln("\t- Resolved functions, got ${this@MutableList}")
        var haveVariable = false
        if (allowVariables) variables[name]?.let {
            add(it)
            haveVariable = true
        }
        logln("\t- Resolved variable, got ${this@MutableList}")

        if (localParent != null) logln("... Continuing to local parent")
        else if (globalParent != null) logln("... Continuing to global parent")
        else logln("... Root reached, nothing else to try, result: ${this@MutableList}")

        if (localParent != null) localParent.resolve(name, allowVariables && !haveVariable)
        else globalParent?.resolve(name, allowVariables && !haveVariable)
    }

    fun resolveVariableOrNull(name: String): Variable? = resolveOrNull(name)?.filterIsInstance<Variable>()?.firstOrNull()

    fun resolve(name: String, location: Location) = resolveOrNull(name) ?: location.error("Unresolved reference `$name`")
    fun resolveVariable(name: String, location: Location) = resolveVariableOrNull(name) ?: location.error("Unresolved reference `$name`")


    fun <E : Element> define(name: String, element: E): E {
        when (element) {
            is Variable -> variables[name] = element.also { logln("Defining variable $name (${element::class.qualifiedName}) into $path") }
            is Function -> functions.getOrPut(name, ::mutableListOf).add(element)
        }
        if (element is Compilable) compilables.add(element)
        if (element is Variable.Global) globals.add(element)
        return element
    }


    val intrinsicTypes = mutableMapOf<String, MutableList<DeclaredType.Intrinsic>>()
    private val types = mutableMapOf<String, MutableList<DeclaredType>>()

    fun resolveTypesOrNull(name: String): List<DeclaredType>? {
        val result = buildList {
            addAll(resolveLocalTypes(name))
            globalParent?.resolveTypesOrNull(name)?.let(::addAll)
        }
        return if (result.isEmpty()) null else result
    }
    private fun resolveLocalTypes(name: String): List<DeclaredType> = (types[name] ?: emptyList()) + (localParent?.resolveLocalTypes(name) ?: emptyList())
    fun resolveTypes(name: String, location: Location): List<DeclaredType> =
        resolveTypesOrNull(name) ?: location.error("Unresolved type `$name`")


    fun defineIntrinsicType(name: String, type: DeclaredType.Intrinsic): DeclaredType.Intrinsic {
        intrinsicTypes.getOrPut(name, ::mutableListOf).add(type)
        return type
    }
    init {
        // This could be done by hand but yea
        defineIntrinsicType("Any", ActualType.Any.declared as DeclaredType.Intrinsic)
        defineIntrinsicType("Void", ActualType.Void.declared as DeclaredType.Intrinsic)
        defineIntrinsicType("Nothing", ActualType.Nothing.declared as DeclaredType.Intrinsic)

        defineIntrinsicType("Byte", ActualType.Byte.declared as DeclaredType.Intrinsic)
        defineIntrinsicType("I32", ActualType.I32.declared as DeclaredType.Intrinsic)
        defineIntrinsicType("I64", ActualType.I64.declared as DeclaredType.Intrinsic)
        defineIntrinsicType("Bool", ActualType.Bool.declared as DeclaredType.Intrinsic)
    }

    fun defineType(name: String, type: DeclaredType): DeclaredType {
        types.getOrPut(name, ::mutableListOf).add(type)
        return type
    }


    val intrinsicFunctions: MutableMap<String, MutableList<Function.Intrinsic>> = globalParent?.intrinsicFunctions ?: mutableMapOf()
    fun resolveIntrinsicFunction(name: String, signature: Function.Signature): Function.Intrinsic {
        val list = intrinsicFunctions.getOrPut(name, ::mutableListOf)

        //TODO: Validate the declaration somehow ig?

        // Check if there is already an intrinsic with a matching signature
        list.forEach { intrinsic ->
            //TODO: This is kinda dumb i should probably do it in some better way at some point but it doesn't really matter for now
            if (intrinsic.signature.returnType != signature.returnType) return@forEach
            if (intrinsic.signature.arguments.size != signature.arguments.size) return@forEach
            intrinsic.signature.arguments.zip(signature.arguments) { existing, actual ->
                //TODO: If this works im gonna be surprised
                if (existing.name != actual.name || existing.type != actual.type) return@forEach
            }
            return intrinsic
        }

        // Otherwise make a new one and return it
        val result = Function.Intrinsic(name, signature)
        list.add(result)
        return result
    }

    private val labelStack: Deque<Label> = localParent?.labelStack ?: LinkedList()
    fun resolveLabel(type: Label.Type, name: String? = null, location: Location): Label {
        return labelStack.firstOrNull { it.type == type && (name == null || it.name == name) } ?: location.error("Unresolved label: $type, $name")
    }

    fun createLabel(type: Label.Type, name: String? = null, expectedType: ActualType?) =
        Label(type, name, expectedType).also { labelStack.push(it) }

    fun popLabel(label: Label) {
        if (labelStack.pop() != label) error("Unexpected label pop ($label)")
    }


    private var _nextLocalId: Long = 0
    val nextLocalId: Long get() = localParent?.nextLocalId ?: _nextLocalId++

    private var _nextGlobalId: Long = 0
    val nextGlobalId: Long get() = globalParent?.nextGlobalId ?: _nextGlobalId++


    private var _stringLiteralType: ActualType? = null
    var stringLiteralType: ActualType?
        get() = globalParent?.stringLiteralType ?: _stringLiteralType
        set(value) = when {
            globalParent != null -> globalParent._stringLiteralType = value
            else -> _stringLiteralType = value
        }


    sealed interface Element

    interface Compilable {
        fun compile()
    }


    fun dump() {
        if (!enableLogging) return
        logln("-- Scope $path --")
        logln("\t- variables: ${variables.keys.joinToString()}")
        logln("\t- functions: ${functions.entries.joinToString { (name, functions) -> "$name (${functions.size}x)" }}")
        when {
            localParent != null -> {
                logln("\t- Local parent follows...")
                localParent.dump()
            }
            globalParent != null -> {
                logln("\t- Global parent follows...")
                globalParent.dump()
            }
            else -> logln("\t- This is the root")
        }
    }
}


data class Label(
    val type: Type,
    val name: String?,
    val expectedType: ActualType?,
    val actualTypes: MutableList<ActualType> = mutableListOf(),
) {
    enum class Type { RETURN, BREAK }
}


fun Scope.include(ast: List<Expression>) {
    val (types, otherElements) = ast.partition { it is Expression.Type }
    val elements = otherElements.toMutableList()
    @Suppress("UNCHECKED_CAST")
    includeTypes(types as List<Expression.Type>, elements::add)
    includeElements(elements)
}

context(Scope) fun includeTypes(types: List<Expression.Type>, deferElement: (Expression) -> Unit) {
    val queue: Deque<Expression.Type> = LinkedList(types)
    var forbiddenPoint: Expression.Type? = null

    while (queue.isNotEmpty()) {
        val type = queue.removeFirst()
        if (type == forbiddenPoint) type.location.error("Unresolvable generics! This is either because the type doesn't exist, or there was a loop in the generics")
        if (type.include(deferElement)) forbiddenPoint = null
        else {
            queue.add(type)
            if (forbiddenPoint == null) forbiddenPoint = type
        }
    }
}

context(Scope) fun includeElements(elements: List<Expression>) {
    elements.forEach { it.include() }
}
context(Scope) fun Expression.include() {
    when (this) {
        is Expression.Property -> {
            //TODO: Handle tags somehow
            val prefix = "\$prop\$$name\$"
            accessors.forEach { accessor ->
                val actualAccessor = accessor.copy(
                    name = "$prefix${accessor.name}",
                    returnType =
                        if (accessor.name == "get" && accessor.returnType == null) type
                        else accessor.returnType,
                )
                actualAccessor.include()
            }
        }
        is Expression.Function -> include()
        is Expression.Variable -> include()
        else -> location.error("Unexpected top-level expression: $this")
    }
}


context(Scope) fun ActualType.resolveFieldOrNull(name: String): Field.Actual? {
    val field = declared.resolveField(name) ?: return null
    val actualType = field.type.fillGenerics { generic ->
        val index = declared.parameters.indexOfFirst { it == generic }
        parameters[index] ?: error("Invalid generics")
    }
    return Field.Actual(field, actualType)
}
