package knife

import kotlin.system.exitProcess


data class Source(
    val name: String,
    val content: String,
)

data class Location(
    val source: Source,
    val index: Int,
) {
    override fun toString(): String {
        val sub = source.content.substring(0, minOf(index, source.content.length))
        val line = sub.count { it == '\n' } + 1
        val column = sub.substringAfterLast('\n').length + 1
        return "${source.name}:$line:$column"
    }

    fun error(message: String): Nothing {
        System.err.println(CompilationError().stackTraceToString())
        System.err.println("[ERROR] $this: $message")
        exitProcess(1)
    }
}
private class CompilationError() : Exception()

data class Token(
    val type: Type,
    val value: String,
    val location: Location,
) {
    private val printValue get() = value.replace("\n", "\\n")

    fun error(message: String): Nothing = location.error(message)
    fun unexpected(): Nothing = error("Unexpected ${type.name.lowercase()} `$printValue`")
    fun unexpected(message: String): Nothing = error("Unexpected ${type.name.lowercase()} `$printValue`, $message")

    fun matches(type: Type?, value: String?) = (type == null || this.type == type) && (value == null || this.value == value)

    fun assert(
        type: Type? = null,
        value: String? = null,
        message: String = "expected ${type?.name?.lowercase() ?: "token"} `${value?.replace("\n", "\\n") ?: ""}`",
    ): Token {
        if (!matches(type, value)) error("Unexpected ${this.type.name.lowercase()} `$printValue`, $message")
        return this
    }

    enum class Type {
        EOL,
        INDENT,
        SYMBOL,
        IDENTIFIER,
        TAG,
        NUMBER,
        STRING,
    }
}


val Char.isIdentifier get() = isLetterOrDigit() || this == '_' || this == '$'
val Char.isNumber get() = isDigit()

val OPERATOR_CHARS = setOf('<', '>', '+', '-', '/', '*', '=', '%', '!', '&', '|')
val Char.isOperator get() = this in OPERATOR_CHARS

val KEYWORDS = setOf("type", "fun", "when", "var", "val", "loop", "break", "return", "else", "true", "false", "null")
val SYMBOLS = setOf("->", "..", ".", ":", "=", "(", ")", ",", "[", "]", "{", "}", "?")
val SPECIAL_SYMBOLS = setOf("->")

fun String.startsWithSymbol(symbol: String): Boolean {
    if (!startsWith(symbol)) return false
    if (symbol in SPECIAL_SYMBOLS || symbol.any { it !in OPERATOR_CHARS }) return true
    return getOrNull(symbol.length) !in OPERATOR_CHARS
}

fun Source.tokenize(): List<Token> {
    val result = mutableListOf<Token>()

    var cursor = 0
    fun Location() = Location(source = this, index = cursor)
    fun error(message: String): Nothing = Location().error(message)

    fun isEOF() = cursor >= content.length
    fun current() = content.getOrNull(cursor)
    fun next() = content.getOrNull(cursor + 1)

    while (!isEOF()) {
        val location = Location()
        val char = current()!!
        when {
            char == '\n' -> {
                while (result.lastOrNull()?.type in setOf(Token.Type.INDENT, Token.Type.EOL)) result.removeLastOrNull()
                /*if (result.lastOrNull()?.type == Token.Type.EOL) {
                    cursor++
                    continue
                }*/
                result.add(Token(Token.Type.EOL, char.toString(), location))

                var indentStart = ++cursor
                var location = Location()
                while (current()?.isWhitespace() == true) {
                    if (current() == '\n') {
                        indentStart = ++cursor
                        location = Location()
                    } else cursor++
                }
                if (indentStart != cursor) result.add(Token(Token.Type.INDENT, content.substring(indentStart, cursor), location))
            }

            char.isWhitespace() -> {
                while (current()?.let { it.isWhitespace() && it != '\n' } == true) cursor++
                while (result.lastOrNull()?.type in setOf(Token.Type.INDENT, Token.Type.EOL)) result.removeLastOrNull()
            }

            char == '/' && next() == '/' -> {
                while (current() != '\n') cursor++
            }
            char == '/' && next() == '*' -> {
                cursor++
                val start = cursor
                while (current() == '*') cursor++
                val magnitude = cursor - start
                if (magnitude % 2 == 0 && current() == '/') cursor++
                else {
                    var closed = 0
                    while (true) {
                        when (current()) {
                            '*' -> closed++
                            '/' -> {
                                if (closed == magnitude) break
                                closed = 0
                            }
                            else -> closed = 0
                        }
                        cursor++
                    }
                    cursor++
                }
                while (current()?.let { it.isWhitespace() && it != '\n' } == true) cursor++
            }

            char == '"' -> {
                val start = cursor
                while (current() == '"') cursor++
                val magnitude = cursor - start

                if (magnitude % 2 != 0) {
                    var closed = 0
                    while (true) {
                        when (current()) {
                            '"' -> closed++
                            '\\' -> closed = -1
                            else -> closed = 0
                        }
                        cursor++
                        if (closed == magnitude) break
                    }
                }

                result.add(Token(Token.Type.STRING, content.substring(start, cursor), location))
            }

            char == '#' -> {
                val start = ++cursor
                if (content[start].let { !it.isIdentifier || it.isNumber }) error("Expected and identifier, got `${current()}`")
                cursor++
                while (current()?.isIdentifier == true) cursor++
                result.add(Token(Token.Type.TAG, content.substring(start, cursor), location))
            }

            content.substring(cursor).let { content -> SYMBOLS.any { content.startsWithSymbol(it) } } -> {
                val sub = content.substring(cursor)
                val symbol = SYMBOLS.first { sub.startsWith(it) }
                cursor += symbol.length
                result.add(Token(Token.Type.SYMBOL, symbol, location))
            }

            char.isOperator -> {
                val start = cursor++
                while (current()?.isOperator == true) cursor++
                result.add(Token(Token.Type.IDENTIFIER, content.substring(start, cursor), location))
            }

            char.isNumber -> {
                val start = cursor++
                while (current()?.isIdentifier == true) cursor++
                result.add(Token(Token.Type.NUMBER, content.substring(start, cursor), location))
            }

            char.isIdentifier -> {
                val start = cursor++
                while (current()?.isIdentifier == true) cursor++
                val value = content.substring(start, cursor)
                result.add(Token(
                    type = if (value in KEYWORDS) Token.Type.SYMBOL else Token.Type.IDENTIFIER,
                    value, location,
                ))
            }

            else -> location.error("Unexpected token `$char`")
        }
    }

    if (result.isNotEmpty() && result.last().type != Token.Type.EOL) result.add(Token(Token.Type.EOL, "\n", Location()))
    return result
}
