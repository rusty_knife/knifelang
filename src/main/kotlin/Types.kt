package knife


data class Field(
    val name: String,
    val id: Long,
    val type: ActualType,
) {
    data class Actual(
        val declared: Field,
        val actualType: ActualType,
    )
}

sealed interface DeclaredType {
    val name: String
    val parameters: List<Generic>
    val primitive: Boolean get() = false

    fun resolveField(name: String): Field? = null

    data class Actual(
        override val name: String,
        override val parameters: List<Generic>,
        val fields: List<Field>,
    ) : DeclaredType {
        override fun resolveField(name: String) = fields.firstOrNull { it.name == name }
    }

    interface Intrinsic : DeclaredType {
        override val parameters get() = emptyList<Generic>()
        override val primitive: Boolean get() = true
    }

    data class Generic(
        override val name: String,
        val bounds: ActualType = ActualType.NullableAny,
    ) : DeclaredType {
        override val parameters get() = emptyList<Generic>()
    }
}

sealed interface ActualType {
    val real: Real
    val declared: DeclaredType
    val nullable: Boolean
    val parameters: List<ActualType?>

    infix fun accepts(other: ActualType): Boolean

    fun nullable(nullable: Boolean): ActualType


    data class Real(
        override val declared: DeclaredType,
        override val nullable: Boolean,
        override val parameters: List<ActualType?>,
    ) : ActualType {
        override val real: Real get() = this
        override fun equals(other: Any?): Boolean =
            other is ActualType
            && other.declared == declared
            && other.parameters.zip(parameters).all { it.first == it.second } //TODO: Does this work?

        override fun accepts(other: ActualType): Boolean {
            //TODO: Do this properly
            if (other.nullable && !this.nullable) return false
            if (this == Any || this == NullableAny) return true
            if (this.declared != other.declared) return false
            return parameters.zip(other.parameters).all { (it.first ?: Any) accepts (it.second ?: Any) }
        }

        override fun nullable(nullable: Boolean) = if (this.nullable == nullable) this else copy(nullable = nullable)

        override fun hashCode(): Int { throw UnsupportedOperationException("ActualType can't be hashed") }

        override fun toString(): String = buildString {
            append(declared.name)
            if (parameters.isNotEmpty()) {
                append("{")
                append(parameters.joinToString { it?.toString() ?: "*" })
                append("}")
            }
        }
    }

    class Deferred(private val shouldBeNullable: Boolean?, private val _real: Lazy<Real>) : ActualType {
        constructor(resolve: () -> ActualType) : this(shouldBeNullable = null, _real = lazy { resolve().real })

        override val real by when (shouldBeNullable) {
            null -> _real
            else -> lazy { _real.value.nullable(shouldBeNullable) }
        }

        override val declared get() = real.declared
        override val nullable get() = real.nullable
        override val parameters get() = real.parameters

        override fun equals(other: Any?) = real == other
        override fun accepts(other: ActualType) = real accepts other
        override fun hashCode() = real.hashCode()
        override fun toString() = if (_real.isInitialized()) real.toString() else "<unresolved>"

        override fun nullable(nullable: Boolean) = when {
            _real.isInitialized() -> real.nullable(nullable)
            shouldBeNullable == nullable -> this
            else -> Deferred(nullable, _real)
        }
    }

    companion object {
        operator fun invoke(declared: DeclaredType, nullable: Boolean, parameters: List<ActualType?> = emptyList()): ActualType =
            Real(declared, nullable, parameters)

        // These are the intrinsic types required by the compiler
        val Any by lazy { (object : DeclaredType.Intrinsic { override val name = "Any" }).toActualOrNull()!! }
        val NullableAny by lazy { Any.nullable(true) }
        val Void by lazy { (object : DeclaredType.Intrinsic { override val name = "Void" }).toActualOrNull()!! }
        val NullableVoid by lazy { Void.nullable(true) }
        val Nothing by lazy { (object : DeclaredType.Intrinsic { override val name = "Nothing" }).toActualOrNull()!! }

        // These are the intrinsic types that have literals - this will probably change a lot
        val Byte by lazy { (object : DeclaredType.Intrinsic { override val name = "Byte" }).toActualOrNull()!! }
        val I32 by lazy { (object : DeclaredType.Intrinsic { override val name = "I32" }).toActualOrNull()!! }
        val I64 by lazy { (object : DeclaredType.Intrinsic { override val name = "I64" }).toActualOrNull()!! }
        val Bool by lazy { (object : DeclaredType.Intrinsic { override val name = "Bool" }).toActualOrNull()!! }
    }
}

fun DeclaredType.toActualOrNull(parameters: List<ActualType?>, nullable: Boolean = false): ActualType? {
    if (parameters.size != this.parameters.size) error("Incorrect number of parameters for this type")
    val boundsValid = this.parameters.zip(parameters).all { (declared, actual) ->
        if (declared.bounds == ActualType.Any) true
        else TODO("Check that the types are compatible")
    }
    if (!boundsValid) return null
    return ActualType(this, nullable, parameters)
}
fun DeclaredType.toActualOrNull(vararg parameters: ActualType?, nullable: Boolean = false) = toActualOrNull(parameters.toList(), nullable)

fun DeclaredType.toActual(parameters: List<ActualType?>, location: Location): ActualType = toActualOrNull(parameters) ?: location.error("Type parameters not within bounds")
fun DeclaredType.toActual(vararg parameters: ActualType?, location: Location): ActualType = toActualOrNull(*parameters) ?: location.error("Type parameters not within bounds")


context(Scope) fun Expression.TypeReference.resolveOrNull(allowInnerFaults: Boolean = true): ActualType? {
    val positional = mutableListOf<ActualType?>()
    val named = mutableMapOf<String, ActualType?>()
    parameters.forEach { parameter ->
        val type = when {
            parameter.value.name == "*" -> null
            allowInnerFaults -> parameter.value.resolveOrNull(allowInnerFaults) ?: return null
            else -> parameter.value.resolve()
        }
        if (parameter.name != null) {
            if (parameter.name in named) parameter.location.error("Type parameter `${parameter.name}` was defined multiple times")
            named[parameter.name] = type
        } else positional.add(type)
    }

    return resolveTypesOrNull(name)?.asSequence()
        ?.mapNotNull { declaration ->
            val positional = positional.toMutableList()
            val named = named.toMutableMap()

            val actualParameters = declaration.parameters.map { parameter -> when {
                parameter.name in named -> named.remove(parameter.name)
                positional.isEmpty() -> return@mapNotNull null
                else -> positional.removeFirst()
            } }

            if (named.isNotEmpty() || positional.isNotEmpty()) return@mapNotNull null

            declaration.toActualOrNull(actualParameters, nullable = nullable)
        }?.singleOrNull()
}

context(Scope) fun Expression.TypeReference.resolve(): ActualType = resolveOrNull() ?: location.error("Unresolved type: `${name}`")


context(Scope) fun Expression.Type.include(deferElement: (element: Expression) -> Unit): Boolean {
    val intrinsic = tags.any { it.name == "intrinsic" }
    if (intrinsic) {
        val intrinsics = intrinsicTypes[name] ?: location.error("Unknown intrinsic type: `$name`")
        val type = intrinsics.firstOrNull { intrinsic -> when {
            intrinsic.parameters.size != genericTypes.size -> false
            else -> intrinsic.parameters.zip(genericTypes).all { (intrinsic, declared) -> when {
                intrinsic.name != declared.name -> false
                else -> {
                    val declaredBounds =
                        if (declared.bounds != null) declared.bounds.resolveOrNull() ?: return false
                        else ActualType.Any

                    declaredBounds == intrinsic.bounds
                }
            } }
        } } ?: return false
        defineType(name, type)
        return true
    } else {
        val parameters = genericTypes.map { generic ->
            val bounds = generic.bounds?.run { resolveOrNull() ?: return false } ?: ActualType.Any
            DeclaredType.Generic(generic.name, bounds)
        }

        val constructorScope = subscope("type $name constructor", keepLocals = false)
        val constructorSignature = with(constructorScope) { generateConstructorSignature(parameters) }

        val type = DeclaredType.Actual(
            name, parameters,
            fields = constructorSignature.arguments.map { argument ->
                Field(argument.name, id = argument.id, ActualType.Deferred { argument.type })
            },
        )
        defineType(name, type)

        val constructor = Function(
            location, constructorScope, name,
            signature = constructorSignature,
            body = Function.Body(
                constructorScope,
                source = null,
                declaredReturnType = constructorSignature.returnType,
                isConstructor = true,
            ),
            tags = emptyList(),
        )
        define(constructor.name, constructor)

        val thisType = generateReference(parameters)

        functions.forEach { function ->
            val actualFunction = function.copy(
                genericTypes = function.genericTypes + genericTypes,
                receiverType = thisType,
            )
            deferElement(actualFunction)
        }

        properties.forEach { property ->
            val actualProperty = property.copy(
                receiverType = thisType,
                accessors = property.accessors.map { accessor ->
                    accessor.copy(
                        genericTypes = genericTypes,//TODO: This should probably be handled elsewhere
                        receiverType = thisType,
                    )
                },
            )
            deferElement(actualProperty)
        }

        return true
    }
}


context(Scope) fun Expression.Type.generateConstructorSignature(generics: List<DeclaredType.Generic>): Function.Signature {
    generics.forEach { generic -> defineType(generic.name, generic) }

    logln("Generating constructor arguments")
    val arguments = mutableListOf<Function.Argument>()
    fields.forEach { field ->
        logln("Generating constructor argument for field $field")
        val declaredType = field.type?.let { ActualType.Deferred { it.resolve() } }
        val id = nextLocalId

        val default = field.initializer?.let { initializer ->
            logln("\t- Generating default")
            val scope = subscope("default of `${field.name}` in `$name`")
            val name = "\$default\$${name}\$$id"

            @Suppress("NAME_SHADOWING")
            val arguments = arguments.map { original ->
                Function.Argument(
                    original.name,
                    id = scope.nextLocalId,
                    declaredType = original.declaredType ?: ActualType.Deferred { original.type },
                    mutable = false,
                    default = null,
                ).also { scope.define(it.name, it) }
            }

            val signature = Function.Signature(generics, arguments, declaredType)

            val bodyScope = scope.subscope("fun `$name` (body)")
            val body = Function.Body(
                scope = bodyScope,
                source = listOf(Expression.Return(initializer.location, initializer)),
                declaredReturnType = declaredType,
            )
            // Constructors don't need to be built, but i'll leave this here anyway //bodyScope.compilables.add(body)
            define(name, Function(field.location, scope, name, signature, body, tags = emptyList()))
        }

        logln("\t- Generating the actual argument...")
        val result = Function.Argument(field.name, id, declaredType, mutable = false, default)
        define(field.name, result)
        arguments.add(result)
    }

    //TODO: This is weird - this should probably get some kind of handle that will give it the included type and construct the actual type from that
    val returnType = ActualType.Deferred { generateReference(generics).resolve() }

    return Function.Signature(generics, arguments, returnType)
}

private fun Expression.Type.generateReference(generics: List<DeclaredType.Generic>): Expression.TypeReference =
    Expression.TypeReference(
        location, name,
        parameters = generics.map { generic ->
            val value = Expression.TypeReference(location, generic.name, parameters = emptyList(), nullable = generic.bounds.nullable)
            Expression.TypeReference.Parameter(location, generic.name, value)
        },
        nullable = false,
    )
