package knife


data class Parser(val tokens: List<Token>) {
    private var restoring = false
    var currentIndent: String = ""
        private set
    var cursor = 0
        set(value) {
            if (restoring) {
                field = value
                return
            }
            val consumed = tokens.subList(field, value)
            field = value
            if (consumed.any { it.type == Token.Type.EOL }) currentIndent = ""
            consumed.lastOrNull { it.type == Token.Type.INDENT }?.let { currentIndent = it.value }

            /*consumed.forEach { when (it.type) {
                Token.Type.EOL -> logln("\\n")
                Token.Type.INDENT -> log("`${it.value.replace("\t", "\\t")}` ")
                Token.Type.TAG -> log("#${it.value} ")
                else -> log("${it.value} ")
            } }
            System.out.flush()*/
        }

    fun snapshot() = Snapshot(cursor, currentIndent)
    inner class Snapshot(private val cursor: Int, private val currentIndent: String) {
        fun restore() {
            restoring = true
            this@Parser.cursor = cursor
            this@Parser.currentIndent = currentIndent
            restoring = false
        }
    }

    val currentOrNull get() = tokens.getOrNull(cursor)
    val isAtEOF get() = currentOrNull == null

    val current get() = currentOrNull ?: tokens.last().error("Unexpected EOF")

    val nextOrNull get() = tokens.getOrNull(cursor + 1)
}


/*sealed interface Expression {
    val location: Location

    fun toString(indent: String) = toString()

    data class TypeDeclaration(
        override val location: Location,
        val name: String,
        val generics: List<String>,
        val tags: List<Tag>,
    ) : Expression {
        override fun toString() = buildString {
            append("type")
            if (tags.isNotEmpty()) append(" (", tags.joinToString(), ")")
            append(name)
            if (generics.isNotEmpty()) append(" {", generics.joinToString(), "}")
        }
    }

    data class GlobalValDeclaration(
        override val location: Location,
        val tags: List<Tag>,
        val name: String,
        val type: TypeAnnotation?,
        val initializer: Expression,
        val mutable: Boolean,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "global val $name: $type = ${initializer.toString(indent)}"
    }

    data class ExtensionValDeclaration(
        override val location: Location,
        val tags: List<Tag>,
        val name: String,
        val receiverType: TypeAnnotation,
        val type: TypeAnnotation?,
        val accessors: List<FunctionDeclaration>,
        val mutable: Boolean,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "extension val $receiverType.`$name`: $type\n$indent\t${accessors.joinToString("\n$indent\t") { it.toString("$indent\t") }}"
    }

    data class TypeAnnotation(
        val location: Location,
        val name: String,
        val generics: List<TypeAnnotation>,
    ) {
        override fun toString() = buildString {
            append(name)
            if (generics.isNotEmpty()) append("{", generics.joinToString(), "}")
        }
    }

    data class NumericLiteral(
        override val location: Location,
        val value: String,
    ) : Expression {
        override fun toString() = "Num($value)"
    }

    data class StringLiteral(
        override val location: Location,
        val value: String,
        val quote: String,
    ) : Expression {
        override fun toString() = "String($quote${value.replace("\n", "\\n")}$quote)"
    }

    data class FunctionDeclaration(
        override val location: Location,
        val name: String,
        val tags: List<Tag>,
        val generics: List<String>,
        val arguments: List<Argument>,
        val returnType: TypeAnnotation?,
        val body: List<Expression>?,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String): String {
            val headerBase = "fun ${ if (tags.isNotEmpty()) "(${tags.joinToString()}) " else "" }$name"
            val header = (
                    if (arguments.isEmpty()) "$headerBase()"
                    else "$headerBase(\n$indent\t${arguments.joinToString(separator = "\n$indent\t") {it.toString("$indent\t")}}\n$indent)"
                    ) + ": $returnType"
            if (body == null) return header
            else return "$header -> {\n$indent\t${body.joinToString("\n$indent\t") {it.toString("$indent\t")}}\n$indent}"
        }

        data class Argument(
            val location: Location,
            val name: String,
            val type: TypeAnnotation?,
            val default: Expression?,
            val mutable: Boolean,
        ) {
            fun toString(indent: String) = (if (mutable) "var" else "") + " $name: $type" + (default?.let { " = ${default.toString(indent)}" } ?: "")
            override fun toString() = toString("")
        }
    }

    data class NameLookup(
        override val location: Location,
        val name: String,
    ) : Expression {
        override fun toString() = "`$name`"
    }

    data class Call(
        override val location: Location,
        val lhs: Expression,
        val arguments: Arguments,
    ) : Expression {
        constructor(location: Location, lhs: Expression, arguments: List<Pair<String?, Expression>>) : this(location, lhs, Arguments(arguments))

        override fun toString() = toString("")
        override fun toString(indent: String): String {
            val lhs = lhs.toString(indent)
            val arguments = arguments.toString("$indent\t")

            return if (arguments.isEmpty()) "$lhs()" else "$lhs(\n$indent\t$arguments\n$indent)"
        }

        data class Arguments(
            val positional: List<Expression>,
            val named: Map<String, List<Expression>>,
        ) {
            constructor(arguments: List<Pair<String?, Expression>>) : this(
                positional = arguments.asSequence()
                    .filter { it.first == null }
                    .map { it.second }
                    .toList(),
                named = arguments.asSequence()
                    .filter { it.first != null }
                    .let { it as Sequence<Pair<String, Expression>> }
                    .groupBy { it.first }
                    .mapValues { it.value.map { it.second } },
            )

            override fun toString() = toString("")
            fun toString(indent: String): String {
                val positional =
                    if (positional.isEmpty()) null
                    else positional.joinToString(separator = "\n$indent") { it.toString("$indent\t") + "," }

                val deepIndent = "$indent\t"
                val named =
                    if (named.isEmpty()) null
                    else named.entries.joinToString(separator = "\n$indent") { (name, values) -> """
                        |$name =
                        |$deepIndent${values.joinToString("\n$deepIndent") { it.toString(deepIndent) + "," }}
                    """.trimMargin() }

                return when {
                    positional == null && named == null -> ""
                    positional != null && named != null -> "$positional\n$indent$named"
                    else -> positional ?: named ?: error("Impossible")
                }
            }
        }
    }

    data class Assignment(
        override val location: Location,
        val lhs: Expression,
        val rhs: Expression,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "${lhs.toString(indent)} = ${rhs.toString(indent)}"
    }

    data class DotLookup(
        override val location: Location,
        val lhs: Expression,
        val name: String,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "${lhs.toString(indent)}.`$name`"
    }

    data class Loop(
        override val location: Location,
        val body: List<Expression>,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "loop {\n$indent\t${body.joinToString("\n$indent\t") {it.toString("$indent\t")}}\n$indent}"
    }

    data class When(
        override val location: Location,
        val branches: List<Branch>,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "when {\n$indent\t${branches.joinToString("\n$indent\t") {it.toString("$indent\t")}}\n$indent}"

        data class Branch(
            val condition: Expression?,
            val body: List<Expression>,
        ) {
            override fun toString() = toString("")
            fun toString(indent: String) = (condition?.toString(indent) ?: "else") + " -> {\n$indent\t${body.joinToString("\n$indent\t") {it.toString("$indent\t")}}\n$indent}"
        }
    }

    data class Return(
        override val location: Location,
        val value: Expression?,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "return" + (value?.let { " ${it.toString(indent)}" } ?: "")
    }

    data class Break(
        override val location: Location,
        val value: Expression?,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = "break" + (value?.let { " ${it.toString(indent)}" } ?: "")
    }

    data class LocalDeclaration(
        override val location: Location,
        val name: String,
        val type: TypeAnnotation?,
        val mutable: Boolean,
        val initializer: Expression,
    ) : Expression {
        override fun toString() = toString("")
        override fun toString(indent: String) = (if (mutable) "var" else "val") + " $name: $type = ${initializer.toString(indent)}"
    }

    data class Tag(
        override val location: Location,
        val name: String,
        val arguments: List<Argument>,
    ) : Expression {
        override fun toString() = if (arguments.isEmpty()) name else "$name(${arguments.joinToString()})"

        data class Argument(
            val location: Location,
            val name: String?,
            val value: String,
        ) {
            override fun toString() = if (name != null) "$name = \"$value\"" else "\"$value\""
        }
    }
}*/


fun parse(tokens: List<Token>): List<Expression> = Parser(tokens).parseTopLevel()

fun Parser.parseTopLevel(): List<Expression> {
    val result = mutableListOf<Expression>()
    while (!isAtEOF) {
        result.add(parseTopLevelExpression())
        currentOrNull?.assert(Token.Type.EOL, message = "Expected EOL")
        cursor++
    }
    return result
}

fun Parser.parseTopLevelExpression(tags: List<Expression.Tag>? = null): Expression = when (current.type) {
    Token.Type.TAG -> parseTopLevelExpression((tags ?: emptyList()) + parseTag())
    Token.Type.SYMBOL -> when (val keyword = current.value) {
        "type" -> parseTypeDeclaration(tags ?: emptyList())
        "val", "var" -> parseTopLevelVariableDeclaration(tags ?: emptyList(), mutable = keyword == "var")
        "fun" -> parseFunction(tags ?: emptyList())
        else -> current.unexpected()
    }
    Token.Type.EOL -> {
        cursor++
        parseTopLevelExpression(tags)
    }
    else -> current.unexpected()
}

fun Parser.parseTag(): Expression.Tag {
    current.assert(Token.Type.TAG)
    val name = current.value
    val location = current.location
    cursor++

    val arguments = mutableListOf<Expression.Tag.Argument>()
    if (currentOrNull?.matches(Token.Type.SYMBOL, "(") == true) {
        cursor++
        while (!isAtEOF && !current.matches(Token.Type.SYMBOL, ")")) {
            val location: Location = current.location
            val name = if (current.type == Token.Type.IDENTIFIER) run {
                val name = current.value
                cursor++
                current.assert(Token.Type.SYMBOL, "=", "expected argument value")
                cursor++
                name
            } else null
            val value = parseStringLiteral()
            arguments.add(Expression.Tag.Argument(location, name, value.value))

            if (currentOrNull?.matches(Token.Type.SYMBOL, ",") != true) break
        }
        current.assert(Token.Type.SYMBOL, ")")
        cursor++
    }

    return Expression.Tag(location, name, arguments)
}
operator fun List<Expression.Tag>.contains(name: String) = any { it.name == name }

fun Parser.parseTypeDeclaration(tags: List<Expression.Tag>): Expression {
    current.assert(Token.Type.SYMBOL, "type")
    cursor++
    current.assert(Token.Type.IDENTIFIER, message = "expected type name")
    val name = current.value
    val location = current.location
    cursor++

    val generics =
        if (currentOrNull?.matches(Token.Type.SYMBOL, "{") == true) parseGenericArgumentsDeclaration()
        else emptyList()

    val fields = mutableListOf<Expression.Variable>()
    val functions = mutableListOf<Expression.Function>()
    val properties = mutableListOf<Expression.Property>()

    if (
        currentOrNull?.type == Token.Type.EOL
        && nextOrNull?.type == Token.Type.INDENT
        && nextOrNull!!.value.startsWith(currentIndent)
        && nextOrNull!!.value != currentIndent
    ) parseBlock {
        val expression = parseTopLevelExpression()
        logln("Parsed expression nested in type $name: $expression")
        when (expression) {
            is Expression.Variable -> fields.add(expression)
            is Expression.Function -> when {
                expression.receiverType != null -> expression.location.error("Nested extension functions not supported yet")
                else -> functions.add(expression)
            }
            is Expression.Property -> when {
                expression.receiverType != null -> expression.location.error("Nested extension properties not supported yet")
                else -> properties.add(expression)
            }
            is Expression.Type -> expression.location.error("Nested types are not supported yet")
            else -> expression.location.error("Unexpected expression: $expression")
        }
    }

    return Expression.Type(location, name, tags, generics, fields, functions, properties)
}

fun Parser.parseGenericArgumentsDeclaration(): List<Expression.GenericType> {
    current.assert(Token.Type.SYMBOL, "{")
    cursor++

    val result = mutableListOf<Expression.GenericType>()
    while (!current.matches(Token.Type.SYMBOL, "}")) {
        result.add(parseGenericType())
        cursor++
        if (!current.matches(Token.Type.SYMBOL, ",")) break
        cursor++
    }
    current.assert(Token.Type.SYMBOL, "}")
    cursor++

    return result
}

fun Parser.parseGenericType(): Expression.GenericType {
    current.assert(Token.Type.IDENTIFIER, message = "expected generic type name")
    val name = current.value
    val location = current.location
    //TODO: Parse bounds here
    return Expression.GenericType(location, name, bounds = null)
}

fun Parser.parseTopLevelVariableDeclaration(tags: List<Expression.Tag>, mutable: Boolean): Expression {
    current.assert(Token.Type.SYMBOL, if (mutable) "var" else "val")
    cursor++

    val snapshot = snapshot()
    parseTypeReferenceOrNull()?.let { typeAnnotation ->
        if (currentOrNull?.matches(Token.Type.SYMBOL, ".") != true) {
            snapshot.restore()
            return@let
        }
        cursor++
        return parseExtension(tags, typeAnnotation, mutable)
    }
    return parseVariable(tags, mutable)
}
private fun Parser.parseVariable(tags: List<Expression.Tag>, mutable: Boolean): Expression {
    if (current.matches(Token.Type.SYMBOL, "var") || current.matches(Token.Type.SYMBOL, "val")) cursor++
    current.assert(Token.Type.IDENTIFIER, message = "expected variable name")
    val name = current.value
    val location = current.location
    cursor++

    val type =
        if (current.matches(Token.Type.SYMBOL, ":")) {
            cursor++
            parseTypeReference()
        } else null

    if (!isAtEOF && !current.matches(Token.Type.SYMBOL, "=")) {
        val accessors = parsePropertyAccessors(receiverType = null)
        if (accessors.isNotEmpty()) return Expression.Property(location, name, tags, receiverType = null, type, accessors, mutable)
        else return Expression.Variable(location, name, tags, type, initializer = null, mutable)
    }

    val initializer =
        if (currentOrNull?.matches(Token.Type.SYMBOL, "=") == true) {
            cursor++
            parseValueExpression()
        } else null

    return Expression.Variable(location, name, tags, type, initializer, mutable)
}
private fun Parser.parseExtension(tags: List<Expression.Tag>, receiverType: Expression.TypeReference, mutable: Boolean): Expression {
    current.assert(Token.Type.IDENTIFIER, message = "expected val name")
    val name = current.value
    val location = current.location
    cursor++

    val type =
        if (current.matches(Token.Type.SYMBOL, ":")) {
            cursor++
            parseTypeReference()
        } else null

    val accessors = parsePropertyAccessors(receiverType)

    return Expression.Property(location, name, tags, receiverType, type, accessors, mutable)
}
private fun Parser.parsePropertyAccessors(receiverType: Expression.TypeReference?): List<Expression.Function> =
    collectBlock { parseFunction(tags = null, noFun = true, receiverType) }
        .onEach {
            if (it !is Expression.Function) it.location.error("Unexpected declaration: Expected a val accessor, got $it")
        } as List<Expression.Function>

fun Parser.parseFunction(
    tags: List<Expression.Tag>? = null,
    noFun: Boolean = false,
    receiverType: Expression.TypeReference? = null,
): Expression {
    val tags = tags ?: mutableListOf<Expression.Tag>().apply {
        while (currentOrNull?.type == Token.Type.TAG) add(parseTag())
    }

    var generics = listOf<Expression.GenericType>()

    if (!noFun) {
        current.assert(Token.Type.SYMBOL, "fun")
        cursor++

        if (current.matches(Token.Type.SYMBOL, "{")) generics = parseGenericArgumentsDeclaration()
    }

    val receiver = receiverType ?: run receiver@{
        val snapshot = snapshot()
        val type = parseTypeReferenceOrNull() ?: return@receiver null
        if (!current.matches(Token.Type.SYMBOL, ".")) {
            snapshot.restore()
            return@receiver null
        }
        cursor++
        type
    }

    current.assert(Token.Type.IDENTIFIER, message = "expected function name")
    val name = current.value
    val location = current.location
    cursor++

    val arguments =
        if (current.matches(Token.Type.SYMBOL, "(")) parseFunctionArguments()
        else emptyList()

    val returnType =
        if (current.matches(Token.Type.SYMBOL, ":")) {
            cursor++
            parseTypeReference()
        } else null

    val body =
        if (current.matches(Token.Type.SYMBOL, "->")) {
            cursor++
            collectBlock { parseValueExpression() }
        } else null

    return Expression.Function(location, name, tags, generics, receiver, arguments, returnType, body)
}

/*fun Parser.parseTypeAnnotation(): Expression.TypeAnnotation {
    current.assert(Token.Type.IDENTIFIER, message = "expected type name")
    val typeName = current.value
    val location = current.location
    cursor++
    return Expression.TypeAnnotation(location, typeName)
}*/
fun Parser.parseTypeReference(): Expression.TypeReference = parseTypeReference(error = true)!!
fun Parser.parseTypeReferenceOrNull(): Expression.TypeReference? = parseTypeReference(error = false)
private fun Parser.parseTypeReference(error: Boolean = true): Expression.TypeReference? {
    val snapshot = snapshot()
    fun reset(): Unit? {
        if (error) return null
        snapshot.restore()
        return Unit
    }

    if (current.type != Token.Type.IDENTIFIER) {
        reset() ?: current.unexpected("expected type name")
        return null
    }

    val name = current.value
    val location = current.location
    cursor++

    val arguments = mutableListOf<Expression.TypeReference.Parameter>()
    if (currentOrNull?.matches(Token.Type.SYMBOL, "{") == true) {
        cursor++
        while (currentOrNull?.matches(Token.Type.SYMBOL, "}") == false) {
            val name =
                if (nextOrNull?.matches(Token.Type.SYMBOL, "=") == true) {
                    if (currentOrNull?.type != Token.Type.IDENTIFIER) {
                        reset() ?: current.unexpected()
                        return null
                    }
                    current.assert(Token.Type.IDENTIFIER)
                    current.value.also { cursor += 2 }
                } else null
            val location = currentOrNull?.location ?: run {
                reset() ?: current.unexpected()
                return null
            }
            arguments.add(Expression.TypeReference.Parameter(location, name, parseTypeReference(error) ?: return null))
            if (currentOrNull?.matches(Token.Type.SYMBOL, ",") != true) break
            cursor++
        }
        if (currentOrNull?.matches(Token.Type.SYMBOL, "}") != true) {
            reset() ?: current.unexpected("expected a closing brace")
            return null
        }
        cursor++
    }

    val nullable = currentOrNull?.matches(Token.Type.SYMBOL, "?") == true
    if (nullable) cursor++

    return Expression.TypeReference(location, name, arguments, nullable)
}

fun Parser.parseValueExpressionOrNull(): Expression? = when (current.type) {
    Token.Type.SYMBOL -> when (current.value) {
        "loop" -> parseLoop()
        "break" -> parseBreak()
        "when" -> parseWhen()
        "return" -> parseReturn()
        "val" -> parseVariable(tags = emptyList(), mutable = false)
        "var" -> parseVariable(tags = emptyList(), mutable = true)
        "true", "false" -> parseBooleanLiteral().parseValueExpressionContinuation()
        "null" -> {
            val result = Expression.NullLiteral(current.location)
            cursor++
            result.parseValueExpressionContinuation()
        }
        "(" -> parseBracketedValueExpression().parseValueExpressionContinuation()
        else -> null
    }
    Token.Type.NUMBER -> parseNumericLiteral().parseValueExpressionContinuation()
    Token.Type.STRING -> parseStringLiteral().parseValueExpressionContinuation()
    Token.Type.IDENTIFIER -> {
        val name = current.value
        val location = current.location
        cursor++
        Expression.NameReference(location, name).parseValueExpressionContinuation()
    }
    else -> null
}

fun Parser.parseValueExpression(): Expression = parseValueExpressionOrNull() ?: current.unexpected()

context(Parser) fun Expression.parseValueExpressionContinuation(): Expression {
    if (isAtEOF) return this
    return when {
        current.matches(Token.Type.SYMBOL, "(") -> parseBracketedCall().parseValueExpressionContinuation()
        current.matches(Token.Type.SYMBOL, "..") -> parseDotDotCall() // No continuation possible after this
        current.matches(Token.Type.SYMBOL, "{") -> {
            val (expression, continuationAllowed) = parseCallWithExplicitGenerics()
            if (continuationAllowed) expression.parseValueExpressionContinuation() else expression
        }
        current.matches(Token.Type.SYMBOL, "[") -> {
            val (expression, continuationAllowed) = parseSquareBracketedCall()
            if (continuationAllowed) expression.parseValueExpressionContinuation() else expression
        }
        current.matches(Token.Type.SYMBOL, "=") -> parseAssignment() // No continuation here either
        current.matches(Token.Type.SYMBOL, ".") -> parseDotLookup().parseValueExpressionContinuation()
        current.matches(Token.Type.SYMBOL, "?") -> {
            val expression = Expression.NullCheck(current.location, value = this)
            cursor++
            expression.parseValueExpressionContinuation()
        }
        current.type == Token.Type.IDENTIFIER -> parseInfixCall() // No continuation possible once again wait. no. nononono //TODO: Figure out next line continuations
        else -> this
    }
}

context(Parser) fun Expression.parseDotLookup(): Expression {
    current.assert(Token.Type.SYMBOL, ".")
    cursor++

    current.assert(Token.Type.IDENTIFIER, message = "Expected function or property name")
    val name = current.value
    val location = current.location
    cursor++

    return Expression.DotReference(location, this, name)
}

private fun Parser.parseCallArguments(endBracket: String): List<Expression.Call.Argument> {
    fun skip() {
        // This is very lazy ik
        while (currentOrNull?.type in setOf(Token.Type.EOL, Token.Type.INDENT)) cursor++
    }
    skip()

    val result = mutableListOf<Expression.Call.Argument>()
    while (!current.matches(Token.Type.SYMBOL, endBracket)) {
        result.add(parseCallArgument())
        if (!current.matches(Token.Type.SYMBOL, ",")) break
        cursor++
        skip()
    }
    current.assert(Token.Type.SYMBOL, endBracket)
    cursor++

    return result
}

context(Parser) fun Expression.parseCallWithExplicitGenerics(): Pair<Expression, Boolean> {
    current.assert(Token.Type.SYMBOL, "{")
    val location = current.location
    cursor++

    val generics = mutableListOf<Expression.Call.GenericArgument>()
    while (!isAtEOF && !current.matches(Token.Type.SYMBOL, "}")) {
        val name =
            if (nextOrNull?.matches(Token.Type.SYMBOL, "=") == true) {
                current.assert(Token.Type.IDENTIFIER)
                current.value.also { cursor += 2 }
            } else null
        val type = parseTypeReference()
        generics.add(Expression.Call.GenericArgument(type.location, name, type))

        if (currentOrNull?.matches(Token.Type.SYMBOL, ",") != true) break
        cursor++
    }
    current.assert(Token.Type.SYMBOL, "}")
    cursor++

    return when {
        current.matches(Token.Type.SYMBOL, "(") -> parseBracketedCall(generics) to true
        current.matches(Token.Type.SYMBOL, "..") -> parseDotDotCall(generics) to false
        current.matches(Token.Type.SYMBOL, "[") -> parseSquareBracketedCall(generics)
        else -> Expression.Call(location, this, generics, emptyList()) to true
    }
}

context(Parser) fun Expression.parseBracketedCall(generics: List<Expression.Call.GenericArgument> = emptyList()): Expression {
    current.assert(Token.Type.SYMBOL, "(")
    val location = current.location
    cursor++
    return Expression.Call(location, this, generics, parseCallArguments(")"))
}

context(Parser) fun Expression.parseSquareBracketedCall(generics: List<Expression.Call.GenericArgument> = emptyList()): Pair<Expression, Boolean> {
    current.assert(Token.Type.SYMBOL, "[")
    val location = current.location
    cursor++

    val arguments = parseCallArguments("]")

    if (currentOrNull?.matches(Token.Type.SYMBOL, "=") == true) {
        cursor++
        val value = parseValueExpression()
        return Expression.Call(
            location,
            lhs = Expression.DotReference(location, this, "\$set"),
            generics,
            arguments = arguments + listOf(Expression.Call.Argument(value.location, "\$value", value)),
        ) to false
    } else return Expression.Call(
        location,
        lhs = Expression.DotReference(location, this, "\$get"),
        generics,
        arguments = arguments,
    ) to true
}

context(Parser) fun Expression.parseDotDotCall(generics: List<Expression.Call.GenericArgument> = emptyList()): Expression {
    current.assert(Token.Type.SYMBOL, "..")
    val location = current.location
    cursor++

    val arguments = mutableListOf<Expression.Call.Argument>()

    fun parseArgumentLine() {
        while (!isAtEOF && current.type != Token.Type.EOL) {
            arguments.add(parseCallArgument())
            if (!current.matches(Token.Type.SYMBOL, ",")) break
            cursor++
        }
    }

    when {
        isAtEOF -> return Expression.Call(location, this, generics, arguments)

        current.type != Token.Type.EOL
                || nextOrNull?.type != Token.Type.INDENT
                || !nextOrNull!!.value.startsWith(currentIndent)
                || nextOrNull!!.value == currentIndent
        -> {
            parseArgumentLine()
            return Expression.Call(location, this, generics, arguments)
        }

        else -> {
            val indent = nextOrNull!!.value
            while (indentMatches(indent)) {
                cursor += 2
                parseArgumentLine()
                currentOrNull?.assert(Token.Type.EOL)
            }
            return Expression.Call(location, this, generics, arguments)
        }
    }
}

//TODO: Figure out how whitespace works here?
//TODO: Do this completely differently so operator precedence can be a thing
context(Parser) fun Expression.parseInfixCall(): Expression {
    current.assert(Token.Type.IDENTIFIER)
    val lhs = Expression.NameReference(current.location, current.value)
    cursor++
    val secondArgument = parseValueExpression()
    val arguments = listOf(
        Expression.Call.Argument(location, name = null, value = this),
        Expression.Call.Argument(secondArgument.location, name = null, value = secondArgument),
    )
    return Expression.Call(lhs.location, lhs, explicitGenerics = emptyList(), arguments)
}

fun Parser.parseCallArgument(): Expression.Call.Argument {
    var name: String? = null
    if (current.type == Token.Type.IDENTIFIER && nextOrNull?.matches(Token.Type.SYMBOL, "=") == true) {
        name = current.value
        cursor += 2
    }
    return Expression.Call.Argument(current.location, name, parseValueExpression())
}

context(Parser) fun Expression.parseAssignment(): Expression {
    current.assert(Token.Type.SYMBOL, "=")
    val location = current.location
    cursor++
    return Expression.Assignment(location, this, parseValueExpression())
}

fun Parser.parseNumericLiteral(): Expression {
    current.assert(Token.Type.NUMBER, message = "expected a number")
    val value = current.value
    val location = current.location
    cursor++
    return Expression.NumericLiteral(location, value)
}

fun Parser.parseBooleanLiteral(): Expression {
    if (current.type != Token.Type.SYMBOL) current.error("Unexpected token `${current.value}`, expected a boolean literal")
    val value = when (current.value) {
        "true" -> true
        "false" -> false
        else -> current.error("Unexpected symbol `${current.value}`, expected a boolean literal")
    }
    val location = current.location
    cursor++
    return Expression.BooleanLiteral(location, value)
}

fun Parser.parseStringLiteral(): Expression.StringLiteral {
    current.assert(Token.Type.STRING, message = "expected a string")
    val raw = current.value
    val location = current.location
    cursor++

    val quote = raw.takeWhile { it == '"' }
    if (quote.length == raw.length) return Expression.StringLiteral(location, value = "", quote.substring(quote.length / 2))
    else return Expression.StringLiteral(location, raw.removeSurrounding(quote), quote)
}

fun Parser.parseFunctionArguments(): List<Expression.Function.Argument> {
    current.assert(Token.Type.SYMBOL, "(")
    cursor++

    fun skip() {
        // This is very lazy ik
        while (currentOrNull?.type in setOf(Token.Type.EOL, Token.Type.INDENT)) cursor++
    }
    skip()

    val result = mutableListOf<Expression.Function.Argument>()
    while (!current.matches(Token.Type.SYMBOL, ")")) {
        result.add(parseFunctionArgument())
        if (!current.matches(Token.Type.SYMBOL, ",")) break
        cursor++
        skip()
    }
    current.assert(Token.Type.SYMBOL, ")")
    cursor++

    return result
}

fun Parser.parseFunctionArgument(): Expression.Function.Argument {
    val mutable = current.matches(Token.Type.SYMBOL, "var")
    if (mutable) cursor++

    current.assert(Token.Type.IDENTIFIER, message = "Expected argument name")
    val name = current.value
    val location = current.location
    cursor++

    val type =
        if (current.matches(Token.Type.SYMBOL, ":")) {
            cursor++
            parseTypeReference()
        } else null

    val default =
        if (current.matches(Token.Type.SYMBOL, "=")) {
            cursor++
            parseValueExpression()
        } else null

    return Expression.Function.Argument(location, name, type, default, mutable)
}

private fun Parser.indentMatches(indent: String) =
    currentOrNull?.type == Token.Type.EOL
            && nextOrNull?.type == Token.Type.INDENT
            && nextOrNull!!.value.startsWith(indent)

private fun Parser.indentMatchesExactly(indent: String) =
    currentOrNull?.type == Token.Type.EOL
            && nextOrNull?.type == Token.Type.INDENT
            && nextOrNull!!.value == indent

private inline fun Parser.parseBlock(parser: Parser.() -> Unit) {
    if (!isAtEOF && current.type != Token.Type.EOL) return parser() // Oneliner

    if (
        nextOrNull?.type != Token.Type.INDENT
        || !nextOrNull!!.value.startsWith(currentIndent)
        || nextOrNull!!.value == currentIndent
    ) return // Empty block

    val indent = nextOrNull!!.value
    while (indentMatches(indent)) {
        cursor += 2
        parser()
        currentOrNull?.assert(Token.Type.EOL)
    }
    currentOrNull?.assert(Token.Type.EOL)
}
private inline fun <E : Expression> Parser.collectBlock(parser: Parser.() -> E): List<E> {
    val result = mutableListOf<E>()
    parseBlock { result.add(parser()) }
    return result
}

fun Parser.parseLoop(): Expression {
    current.assert(Token.Type.SYMBOL, "loop")
    val location = current.location
    cursor++
    current.assert(Token.Type.SYMBOL, "->")
    cursor++
    return Expression.Loop(location, collectBlock { parseValueExpression() })
}

fun Parser.parseBreak(): Expression {
    current.assert(Token.Type.SYMBOL, "break")
    val location = current.location
    cursor++
    return Expression.Break(location, parseValueExpressionOrNull())
}

fun Parser.parseWhen(): Expression {
    current.assert(Token.Type.SYMBOL, "when")
    val whenLocation = current.location
    cursor++

    when {
        isAtEOF -> return Expression.When(whenLocation, emptyList())
        current.type != Token.Type.EOL -> {
            val indent = currentIndent
            val branchLocation = current.location
            val condition = parseValueExpression()
            current.assert(Token.Type.SYMBOL, "->", message = "expected branch body")
            cursor++
            val mainBody = collectBlock { parseValueExpression() }

            fun parseElse(): List<Expression> {
                cursor++
                current.assert(Token.Type.SYMBOL, "->", "expected branch body")
                cursor++
                return collectBlock { parseValueExpression() }
            }

            val elseLocation = currentOrNull?.location
            val elseBody = when {
                isAtEOF -> null
                current.matches(Token.Type.SYMBOL, "else") -> parseElse()
                indentMatchesExactly(indent) && tokens.getOrNull(cursor + 2)?.matches(Token.Type.SYMBOL, "else") == true -> {
                    cursor += 2
                    parseElse()
                }
                else -> null
            }

            return Expression.When(whenLocation, listOfNotNull(
                Expression.When.Branch(branchLocation, condition, mainBody),
                elseBody?.let { Expression.When.Branch(elseLocation!!, condition = null, it) },
            ))
        }
        else -> {
            val branches = collectBlock {
                val branchLocation = current.location
                val condition =
                    if (current.matches(Token.Type.SYMBOL, "else")) {
                        cursor++
                        null
                    } else parseValueExpression()
                current.assert(Token.Type.SYMBOL, "->")
                cursor++

                val body = collectBlock { parseValueExpression() }

                Expression.When.Branch(branchLocation, condition, body)
            }
            return Expression.When(whenLocation, branches)
        }
    }
}

fun Parser.parseReturn(): Expression {
    current.assert(Token.Type.SYMBOL, "return")
    val location = current.location
    cursor++
    return Expression.Return(location, parseValueExpressionOrNull())
}

/*fun Parser.parseLocalDeclaration(mutable: Boolean): Expression {
    current.assert(Token.Type.SYMBOL, if (mutable) "var" else "val")
    cursor++

    current.assert(Token.Type.IDENTIFIER, message = "expected local variable name")
    val name = current.value
    val location = current.location
    cursor++

    val type =
        if (current.matches(Token.Type.SYMBOL, ":")) {
            cursor++
            parseTypeReference()
        } else null

    current.assert(Token.Type.SYMBOL, "=", message = "expected local variable initializer")
    cursor++

    //TODO: Allow a line break here ig

    return Expression.LocalDeclaration(location, name, type, mutable, parseValueExpression())
}*/

fun Parser.parseBracketedValueExpression(): Expression {
    current.assert(Token.Type.SYMBOL, "(")
    cursor++

    //TODO: Allow line breaks in here

    val result = parseValueExpression()

    current.assert(Token.Type.SYMBOL, ")", message = "expected closing brace")
    cursor++

    return result
}
