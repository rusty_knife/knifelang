package knife


@OptIn(ExperimentalUnsignedTypes::class)
class AstInterpreter(
    val intrinsicRunner: (intrinsic: Function.Intrinsic, memory: Memory) -> Value,
) {
    private val globals = mutableMapOf<Long, Value>()

    fun initGlobal(global: Variable.Global) {
        globals[global.id] = global.initializer.instruction.execute(Memory())
    }

    fun run(function: Function): Value = try {
        function.execute(Memory())
    } catch (e: Exception) {
        var error: Throwable = e
        val stackTrace = buildList {
            while (error is RuntimeError) {
                add((error as RuntimeError).operation)
                error = error.cause ?: break
            }
        }
        stackTrace.asReversed().forEach { System.err.println(it) }
        throw error
    }


    private fun Ir.Operation.execute(memory: Memory): Value {
        try {
            return when (this) {
                is Ir.Operation.Byte -> Value(ActualType.Byte, value)
                is Ir.Operation.I32 -> Value(ActualType.I32, value)
                is Ir.Operation.I64 -> Value(ActualType.I64, value)
                is Ir.Operation.Bool -> Value(ActualType.Bool, value)
                is Ir.Operation.StringLiteral -> Value(BasicIntrinsics.Array.toActualOrNull(ActualType.Byte)!!, Value.Bytes(value.toByteArray(Charsets.UTF_8).asUByteArray()))
                is Ir.Operation.NullLiteral -> Value.Void

                is Ir.Operation.GlobalValRead -> globals[global.id] ?: error("Attempt to read uninitialized global #${global.id}")

                is Ir.Operation.LocalValInit -> initializer.execute(memory).also { memory[local.id] = it }
                is Ir.Operation.LocalVarInit -> initializer.execute(memory).also { memory[local.id] = it }

                is Ir.Operation.LocalValRead -> memory[local.id]
                is Ir.Operation.LocalVarRead -> memory[local.id]

                is Ir.Operation.LocalVarWrite -> value.execute(memory).also { memory[local.id] = it }

                is Ir.Operation.FieldRead -> {
                    val target = target.execute(memory).value as MutableMap<Long, Value>
                    return target[field.declared.id]!!
                }
                is Ir.Operation.FieldWrite -> {
                    val target = target.execute(memory).value as MutableMap<Long, Value>
                    val value = value.execute(memory)
                    target[field.declared.id] = value
                    return value
                }

                is Ir.Operation.When -> {
                    val branch = branches.firstOrNull {
                        it.condition == null || it.condition.execute(memory).value == true
                    }
                    branch?.body?.forEach { it.execute(memory) }
                    Value.Void
                }
                is Ir.Operation.Loop -> {
                    try {
                        while (true) body.forEach { it.execute(memory) }
                        error("Impossible")
                    } catch (e: BreakException) { e.value }
                }
                is Ir.Operation.Break -> throw BreakException(value?.execute(memory) ?: Value.Void)

                is Ir.Operation.Call -> {
                    // Set up the arguments into temporary locals
                    setup.forEach { it.execute(memory) }

                    // Copy the arguments into the called function's memory
                    val callMemory = Memory()
                    if (function.signature.arguments.size != arguments.size) error("Invalid arguments in call instruction: ${arguments.joinToString { it.name }}")
                    function.signature.arguments.zip(arguments) { argument, local -> callMemory[argument.id] = memory[local.id] }

                    // Actually call the function
                    function.execute(callMemory)
                }
                is Ir.Operation.Return -> throw ReturnException(value?.execute(memory) ?: Value.Void)

                is Ir.Operation.NullCheck -> Value(ActualType.Bool, value.execute(memory) != Value.Void)
            }
        } catch (e: Exception) {
            when (e) {
                is ReturnException, is BreakException -> throw e
                else -> throw RuntimeError(this, e)
            }
        }
    }

    class RuntimeError(val operation: Ir.Operation, override val cause: Exception) : Exception("Error while executing $operation", cause)


    private fun Function.execute(memory: Memory): Value {
        if (body.isConstructor) {
            val data = mutableMapOf<Long, Value>()
            signature.arguments.forEach { argument -> data[argument.id] = memory[argument.id] }
            //TODO: Fill in generics
            return Value(signature.returnType, data)
        }
        when (val body = body.compiled) {
            is Function.Body.Compiled.Intrinsic -> return intrinsicRunner(body.implementation, memory)
            is Function.Body.Compiled.Actual -> {
                try { body.instructions.forEach { it.execute(memory) } }
                catch (e: ReturnException) { return e.value }
                return Value.Void
            }
        }
    }


    data class Value(val type: ActualType, val value: Any?) {
        companion object {
            val Void = Value(ActualType.Void, null)
        }

        data class Bytes(
            val data: UByteArray,
            val offset: Long = 0,
            val size: Long = data.size.toLong(),
        )

        data class GenericArray(
            val data: Array<Value?>,
            val offset: Long = 0,
            val size: Long = data.size.toLong(),
        )
    }

    class ReturnException(val value: Value) : Exception()
    class BreakException(val value: Value) : Exception()

    class Memory {
        private val locals = mutableMapOf<Long, Value>()
        operator fun get(id: Long): Value = locals[id] ?: error("Attempt to read uninitialized local #$id")
        operator fun set(id: Long, value: Value) { locals[id] = value }
    }
}


class IntrinsicMatcher : (Function.Intrinsic, AstInterpreter.Memory) -> AstInterpreter.Value {
    override fun invoke(intrinsic: Function.Intrinsic, memory: AstInterpreter.Memory): AstInterpreter.Value {
        val rootMatcher = matchers[intrinsic.name] ?: error("No known intrinsic with name `${intrinsic.name}`")
        val executor = rootMatcher.match(intrinsic.signature) ?: error("None of the intrinsics named `${intrinsic.name}` matches the given arguments")
        return executor.execute(intrinsic, memory)
    }

    private val matchers = mutableMapOf<String, ArgumentMatcher>()

    private fun add(name: String, vararg arguments: ActualType, executor: Executor) {
        val matcher = arguments.fold(matchers.getOrPut(name, ::ArgumentMatcher)) { matcher, type -> matcher[type] }
        matcher.executor = executor
    }

    private class ArgumentMatcher(val position: Int = 0) {
        var executor: Executor? = null
        private val nodes = mutableListOf<Node>()

        fun match(signature: Function.Signature): Executor? {
            if (signature.arguments.size == position) return executor
            val type = signature.arguments[position].type
            return nodes.firstOrNull { it.type accepts type }?.child?.match(signature)
        }
        operator fun get(type: ActualType) = nodes.firstOrNull { it.type == type }?.child ?: Node(type).also { nodes.add(it) }.child

        private inner class Node(
            val type: ActualType,
            val child: ArgumentMatcher = ArgumentMatcher(position + 1),
        )
    }

    private class Executor(val function: (Arguments) -> AstInterpreter.Value) {
        fun execute(intrinsic: Function.Intrinsic, memory: AstInterpreter.Memory): AstInterpreter.Value {
            val arguments = intrinsic.signature.arguments.map { memory[it.id] }.toTypedArray()
            return function(Arguments(arguments))
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Arguments(private val arguments: Array<AstInterpreter.Value>) {
        operator fun get(index: Int) = arguments[index]

        operator fun <T> component1(): T = arguments[0].value as T
        operator fun <T> component2(): T = arguments[1].value as T
        operator fun <T> component3(): T = arguments[2].value as T
        operator fun <T> component4(): T = arguments[3].value as T
        operator fun <T> component5(): T = arguments[4].value as T
        operator fun <T> component6(): T = arguments[5].value as T
        operator fun <T> component7(): T = arguments[6].value as T
        operator fun <T> component8(): T = arguments[7].value as T
        operator fun <T> component9(): T = arguments[8].value as T
        operator fun <T> component10(): T = arguments[9].value as T
    }


    companion object {
        operator fun invoke(builder: Builder.() -> Unit) = Builder().apply(builder).build()
    }


    class Builder {
        private val result = IntrinsicMatcher()
        fun build() = result

        operator fun String.invoke(vararg parameters: ActualType, executor: (arguments: Arguments) -> AstInterpreter.Value) {
            result.add(this, *parameters, executor = Executor(executor))
        }
    }
}
