package knife


class SignatureMatcher(val signature: Function.Signature) {
    data class GenericArgument(val name: String?, val value: ActualType)
    val explicitGenerics = mutableListOf<GenericArgument>()
    fun addExplicitGeneric(name: String?, value: ActualType) { explicitGenerics.add(GenericArgument(name, value)) }

    data class ArgumentValue(
        val name: String?,
        val compile: (ActualType) -> Ir.Operation?,
        var argument: Function.Argument? = null,
        var value: Ir.Operation? = null,
        var temporaryLocal: Variable.Local? = null,
    )
    val argumentValues = mutableListOf<ArgumentValue>()
    fun addArgumentValue(name: String?, compile: (ActualType) -> Ir.Operation?) { argumentValues.add(ArgumentValue(name, compile)) }


    private var _actualReturnType: ActualType? = null
    val actualReturnType get() = _actualReturnType!!

    val actualArgumentValues = mutableListOf<ArgumentValue>()
    val argumentsInSetupOrder = mutableListOf<ArgumentValue>()


    fun match(makeTemporaryLocal: (name: String, type: ActualType) -> Variable.Local): Boolean {
        val tag = "[${hashCode()}]"
        logln("$tag Matching signature $signature")

        val resolvedGenerics = mutableMapOf<String, ActualType>()
        argumentsInSetupOrder.addAll(argumentValues)

        explicitGenerics.forEach { generic ->
            val declaration = signature.generics.firstOrNull { declaration ->
                declaration.name !in resolvedGenerics && (generic.name == null || generic.name == declaration.name)
            } ?: return false
            if (!(declaration.bounds accepts generic.value)) return false
            resolvedGenerics[declaration.name] = generic.value
        }

        signature.arguments.forEach { declaration ->
            val value =
                argumentValues.firstOrNull { it.value == null && it.name == declaration.name }
                ?: argumentValues.firstOrNull { it.value == null && it.name == null }

            val temporaryLocalName = "\$tmp\$call_argument\$${declaration.name}"

            if (value == null) {
                if (declaration.default == null) return false

                val deferredType = ActualType.Deferred { declaration.default.signature.returnType }
                val value = ArgumentValue(
                    name = declaration.name,
                    compile = { error("How tf did you get here huh?") },
                    argument = declaration,
                    value = Ir.Operation.Call(
                        setup = emptyList(),
                        function = declaration.default,
                        arguments = actualArgumentValues.map { it.temporaryLocal!! },
                        returnType = deferredType,
                    ),
                    temporaryLocal = makeTemporaryLocal(temporaryLocalName, deferredType),
                )
                argumentsInSetupOrder.add(value)
                actualArgumentValues.add(value)

                //TODO: Should i do the generics magic here as well?
            } else {
                val type = declaration.type.fillGenerics { resolvedGenerics[it.name] ?: it.bounds }
                value.argument = declaration
                value.value = value.compile(type) ?: return false
                if (!(type accepts value.value!!.returnType)) return false
                value.temporaryLocal = makeTemporaryLocal(temporaryLocalName, type)
                value.value!!.returnType.extractGenerics(declaration.type) { generic, actual ->
                    if (generic.name !in resolvedGenerics) resolvedGenerics[generic.name] = actual
                }

                actualArgumentValues.add(value)
            }
        }

        logln("$tag Raw return type: ${signature.returnType}")
        _actualReturnType = signature.returnType.fillGenerics { resolvedGenerics[it.name] ?: it.bounds }
        logln("$tag Return type with resolved generics: $_actualReturnType")

        return argumentValues.all { it.value != null }
    }
}


fun ActualType.fillGenerics(lookup: (DeclaredType.Generic) -> ActualType): ActualType = real.run {
    if (declared is DeclaredType.Generic) return lookup(declared)
    return copy(parameters = parameters.map { it?.fillGenerics(lookup) ?: ActualType.Any })
}

private fun ActualType.extractGenerics(declaration: ActualType, callback: (DeclaredType.Generic, ActualType) -> Unit): Unit = real.run {
    val declaration = declaration.real
    if (declaration.declared is DeclaredType.Generic) {
        callback(declaration.declared, this)
        return
    }

    //TODO: Do this properly with actual validations and shi
    parameters.asSequence()
        .zip(declaration.parameters.asSequence())
        .filter { (actual, declared) -> actual != null && declared != null }
        .forEach { (actual, declared) -> actual!!.extractGenerics(declared!!, callback) }
}
