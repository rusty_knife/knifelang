package knife


object Ir {
    sealed interface Operation {
        val returnType: ActualType

        fun coerceInto(expectedType: ActualType): Operation? = when {
            expectedType == returnType -> this
            else -> null
        }


        data class Byte(val value: UByte) : Operation {
            override val returnType = ActualType.Byte
            override fun coerceInto(expectedType: ActualType) = when (expectedType) {
                ActualType.I32 -> I32(value.toInt())
                ActualType.I64 -> I64(value.toLong())
                else -> super.coerceInto(expectedType)
            }
        }
        data class I32(val value: Int) : Operation {
            override val returnType = ActualType.I32
            override fun coerceInto(expectedType: ActualType) = when (expectedType) {
                ActualType.Byte -> {
                    val result = value.toUByte()
                    if (result.toInt() == value) Byte(result)
                    else null
                }
                ActualType.I64 -> I64(value.toLong())
                else -> super.coerceInto(expectedType)
            }
        }
        data class I64(val value: Long) : Operation { override val returnType = ActualType.I64 }

        data class Bool(val value: Boolean) : Operation { override val returnType = ActualType.Bool }

        data class StringLiteral(val value: String, override val returnType: ActualType) : Operation

        data class NullLiteral(override val returnType: ActualType = ActualType.NullableVoid) : Operation {
            override fun coerceInto(expectedType: ActualType) =
                if (!expectedType.nullable) null
                else NullLiteral(expectedType)
        }


        data class LocalValInit(val local: Variable.Local, val initializer: Operation) : Operation {
            override val returnType get() = local.type
        }
        data class LocalValRead(val local: Variable.LocalLike) : Operation {
            override val returnType get() = local.type
        }

        data class LocalVarInit(val local: Variable.Local, val initializer: Operation) : Operation {
            override val returnType get() = local.type
        }
        data class LocalVarRead(val local: Variable.LocalLike) : Operation {
            override val returnType get() = local.type
        }
        data class LocalVarWrite(val local: Variable.LocalLike, val value: Operation) : Operation {
            override val returnType get() = local.type
        }

        data class GlobalValRead(val global: Variable.Global) : Operation {
            override val returnType get() = global.type
        }

        data class FieldRead(val target: Operation, val field: Field.Actual) : Operation {
            override val returnType get() = field.actualType
        }
        data class FieldWrite(val target: Operation, val field: Field.Actual, val value: Operation) : Operation {
            override val returnType get() = field.actualType
        }

        data class NullCheck(val value: Operation) : Operation {
            override val returnType get() = ActualType.Bool
        }

        data class Call(
            val setup: List<Operation>,
            val function: Function,
            val arguments: List<Variable.Local>,
            override val returnType: ActualType,
        ) : Operation {
            override fun toString() = buildString {
                append("Call(")
                append("function = `${function.name}` defined at ${function.location}, ")
                append("arguments = [${arguments.joinToString { it.type.toString() }}], ")
                append("returnType = $returnType")
                append(")")
            }
        }

        data class Loop(val label: Label, val body: List<Operation>, override val returnType: ActualType) : Operation

        data class When(val branches: List<Branch>) : Operation {
            override val returnType get() = ActualType.Void //TODO: Implement when expression
            data class Branch(val condition: Operation?, val body: List<Operation>)
        }

        data class Return(val label: Label, val value: Operation?) : Operation {
            override val returnType get() = ActualType.Nothing
        }

        data class Break(val label: Label, val value: Operation?) : Operation {
            override val returnType get() = ActualType.Nothing
        }
    }
}
