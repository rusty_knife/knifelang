package knife

import java.io.File
import java.io.IOException


class BasicIntrinsics(scope: Scope) {
    init {
        scope.defineIntrinsicType("Array", Array)
    }


    object Array : DeclaredType.Intrinsic {
        override val name = "Array"
        override val parameters = listOf(DeclaredType.Generic("T"))
    }
}


private typealias Bytes = AstInterpreter.Value.Bytes
private typealias GenericArray = AstInterpreter.Value.GenericArray

@OptIn(ExperimentalUnsignedTypes::class)
val intrinsicMatcher = IntrinsicMatcher {
    val Void= AstInterpreter.Value.Void
    val Byte = ActualType.Byte
    val I32 = ActualType.I32
    val I64 = ActualType.I64
    val Bool = ActualType.Bool
    val ArrayOfBytes = BasicIntrinsics.Array.toActualOrNull(ActualType.Byte)!!
    val ErasedArray = BasicIntrinsics.Array.toActualOrNull(ActualType.Any)!!

    fun value(type: ActualType, value: Any?) = AstInterpreter.Value(type, value)
    fun byte(value: UByte) = value(Byte, value)
    fun i32(value: Int) = value(I32, value)
    fun i64(value: Long) = value(I64, value)
    fun bool(value: Boolean) = value(Bool, value)
    fun arrayOfBytes(value: Bytes) = value(ArrayOfBytes, value)
    fun arrayOfAny(value: GenericArray) = value(ErasedArray, value)


    "<="(Byte, Byte) { (left: UByte, right: UByte) -> bool(left <= right) }
    "<="(I32, I32) { (left: Int, right: Int) -> bool(left <= right) }
    "<="(I64, I64) { (left: Long, right: Long) -> bool(left <= right) }
    "<"(Byte, Byte) { (left: UByte, right: UByte) -> bool(left < right) }
    "<"(I32, I32) { (left: Int, right: Int) -> bool(left < right) }
    "<"(I64, I64) { (left: Long, right: Long) -> bool(left < right) }
    "=="(Byte, Byte) { (left: UByte, right: UByte) -> bool(left == right) }
    "=="(I32, I32) { (left: Int, right: Int) -> bool(left == right) }
    "=="(I64, I64) { (left: Long, right: Long) -> bool(left == right) }

    "+"(Byte, Byte) { (left: UByte, right: UByte) -> byte((left + right).toUByte()) }
    "+"(I32, I32) { (left: Int, right: Int) -> i32(left + right) }
    "+"(I64, I64) { (left: Long, right: Long) -> i64(left + right) }
    "-"(Byte, Byte) { (left: UByte, right: UByte) -> byte((left - right).toUByte()) }
    "-"(I32, I32) { (left: Int, right: Int) -> i32(left - right) }
    "-"(I64, I64) { (left: Long, right: Long) -> i64(left - right) }

    "*"(Byte, Byte) { (left: UByte, right: UByte) -> byte((left * right).toUByte()) }
    "*"(I32, I32) { (left: Int, right: Int) -> i32(left * right) }
    "*"(I64, I64) { (left: Long, right: Long) -> i64(left * right) }
    "/"(Byte, Byte) { (left: UByte, right: UByte) -> byte((left / right).toUByte()) }
    "/"(I32, I32) { (left: Int, right: Int) -> i32(left / right) }
    "/"(I64, I64) { (left: Long, right: Long) -> i64(left / right) }
    "%"(Byte, Byte) { (left: UByte, right: UByte) -> byte((left % right).toUByte()) }
    "%"(I32, I32) { (left: Int, right: Int) -> i32(left % right) }
    "%"(I64, I64) { (left: Long, right: Long) -> i64(left % right) }

    "!"(Bool) { (bool: Boolean) -> bool(!bool) }
    "&&"(Bool, Bool) { (left: Boolean, right: Boolean) -> bool(left && right) }
    "||"(Bool, Bool) { (left: Boolean, right: Boolean) -> bool(left || right) }

    "to_byte"(I64) { (value: Long) -> byte(value.toUByte()) }
    "to_i32"(Byte) { (value: UByte) -> i32(value.toInt()) }
    "to_i32"(I64) { (value: Long) -> i32(value.toInt()) }
    "to_i64"(Byte) { (value: UByte) -> i64(value.toLong()) }
    "to_i64"(I32) { (value: Int) -> i64(value.toLong()) }


    "create_byte_array"(I64) { (size: Long) -> arrayOfBytes(Bytes(UByteArray(size.toInt()))) }
    "Array"(I64) { arguments ->
        val (size: Long) = arguments
        //TODO: Make bytes if the generic is Array{Byte}
        arrayOfAny(GenericArray(arrayOfNulls<AstInterpreter.Value?>(size.toInt())))
    }

    "\$prop\$size\$get"(ErasedArray) { (array: Any) -> when (array) {
        is Bytes -> i64(array.size)
        is GenericArray -> i64(array.size)
        else -> error("Not an array type")
    } }

    "\$get"(ErasedArray, I64) { (array: Any, offset: Long) -> when (array) {
        is Bytes -> byte(array.data[array.offset.toInt() + offset.toInt()])
        is GenericArray -> array.data[array.offset.toInt() + offset.toInt()] ?: error("Access to uninitialized array index #$offset")
        else -> error("Not an array type")
    } }
    "\$set"(ErasedArray, I64, ActualType.Any) { arguments ->
        val (array: Any, offset: Long, value: Any?) = arguments
        when (array) {
            is Bytes -> array.data[array.offset.toInt() + offset.toInt()] = (value as UByte)
            is GenericArray -> array.data[array.offset.toInt() + offset.toInt()] = arguments[2]
            else -> error("Not an array type")
        }
        Void
    }

    "view"(ErasedArray, I64, I64) { (array: Any, offset: Long, length: Long) ->
        when (array) {
            is Bytes -> arrayOfBytes(Bytes(array.data, array.offset + offset, length))
            is GenericArray -> arrayOfAny(GenericArray(array.data, array.offset + offset, length))
            else -> error("Not an array type")
        }
    }


    "write"(I32, ArrayOfBytes, I64) { (fd: Int, buffer: Bytes, count: Long) ->
        File("/proc/${ProcessHandle.current().pid()}/fd/$fd").outputStream().use { stream ->
            stream.write(buffer.data.asByteArray(), buffer.offset.toInt(), count.toInt())
        }
        i64(count)
    }

    "read"(I32, ArrayOfBytes, I64) { (fd: Int, buffer: Bytes, count: Long) ->
        try {
            val read = File("/proc/${ProcessHandle.current().pid()}/fd/$fd").inputStream().use { stream ->
                stream.read(buffer.data.asByteArray(), buffer.offset.toInt(), count.toInt())
            }
            if (read < 0) i64(0) else i64(read.toLong())
        } catch (e: IOException) { i64(-1) }
    }

    "sleep"(I64) { (millis: Long) ->
        Thread.sleep(millis)
        Void
    }
}
