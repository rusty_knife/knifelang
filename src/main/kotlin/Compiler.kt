package knife


fun Scope.compile() {
    while (compilables.isNotEmpty()) {
        compilables.removeFirst().compile()
    }

    logln(buildString {
        appendLine("Intrinsics encountered in the compilation:")
        intrinsicFunctions.forEach { (name, overloads) ->
            overloads.forEach { function ->
                appendLine("\t- `${function.name}`(${function.signature})")
            }
        }
    })

    //TODO: Flatten
}


fun Function.compileIntrinsic(): Function.Body.Compiled {
    val intrinsic = rootScope.resolveIntrinsicFunction(name, signature)
    return Function.Body.Compiled.Intrinsic(intrinsic)
}


fun Function.compileActual(): Function.Body.Compiled {
    val returnLabel = body.scope.createLabel(Label.Type.RETURN, name, body.declaredReturnType)
    val instructions = with(body.scope) {
        body.source!!.map { it.compileValueExpression(expectedType = null) }
    }
    body.scope.popLabel(returnLabel)

    val actualReturnType = body.declaredReturnType ?: returnLabel.resolveActualTypeOrNull() ?: location.error("Incompatible return types")
    return Function.Body.Compiled.Actual(instructions, returnType = actualReturnType)
}


fun Label.resolveActualTypeOrNull(): ActualType? = when {
    actualTypes.isEmpty() -> ActualType.Void
    actualTypes.windowed(2).all { (first, second) -> first == second } -> actualTypes.first()
    else -> null
}


context(Scope) fun Expression.Call.Argument.compileValueOrNull(expectedType: ActualType?, onCoerced: () -> Unit = {}): Ir.Operation? =
    value.compileValueExpressionOrNull(expectedType, precompiledValue, onCoerced = onCoerced)

context(Scope) fun Expression.compileValueExpressionOrNull(
    expectedType: ActualType?,
    precompiledResult: Ir.Operation? = null,
    allowNulls: Boolean = true,
    onCoerced: () -> Unit = {},
): Ir.Operation? {
    val result = precompiledResult ?: when (this) {
        is Expression.NumericLiteral -> {
            //TODO: Implement this properly
            val stripped = value.filter { it != '_' }
            stripped.toIntOrNull()?.let(Ir.Operation::I32)
                ?: stripped.toLongOrNull()?.let(Ir.Operation::I64)
                ?: location.error("Malformed numeric literal: `$value`")
        }

        is Expression.Variable -> {
            if (initializer == null) location.error("Local variables have to have an initializer")
            val declaredType = type?.resolve()
            val compiledInitializer = initializer.compileValueExpression(expectedType = declaredType)
            val local = Variable.Local(name, nextLocalId, mutable, declaredType ?: compiledInitializer.returnType)
            define(local.name, local)

            if (mutable) Ir.Operation.LocalVarInit(local, compiledInitializer)
            else Ir.Operation.LocalValInit(local, compiledInitializer)
        }

        is Expression.Call -> {
            val allFunctions = mutableListOf<Function>()
            data class Option(
                val function: Function,
                val match: SignatureMatcher,
                val coercions: Int,
            ) {
                fun generate(): Ir.Operation.Call {
                    val setup = mutableListOf<Ir.Operation>()
                    match.argumentsInSetupOrder.forEach { argument ->
                        setup.add(Ir.Operation.LocalValInit(argument.temporaryLocal!!, argument.value!!))
                    }
                    return Ir.Operation.Call(
                        setup = match.argumentsInSetupOrder.map { argument ->
                            Ir.Operation.LocalValInit(argument.temporaryLocal!!, argument.value!!)
                        },
                        function,
                        arguments = match.actualArgumentValues.map { it.temporaryLocal!! },
                        returnType = match.actualReturnType,
                    )
                }
            }
            val options = mutableListOf<Option>()

            fun resolveSimpleCalls(name: String, arguments: List<Expression.Call.Argument>) {
                //TODO: Look up functions for contexts

                resolveOrNull(name)
                    ?.filterIsInstance<Function>()
                    ?.forEach { function ->
                        allFunctions.add(function)

                        val scope = subscope("call `${function.name}`")
                        val matcher = SignatureMatcher(function.signature)
                        logln("Setting up matching with tag ${matcher.hashCode()} for $function")
                        //TODO: Generics ig? how are there no generics in the call notation huh?
                        var coercions = 0
                        explicitGenerics.forEach { generic ->
                            matcher.addExplicitGeneric(generic.name, generic.type.resolve())
                        }
                        arguments.forEachIndexed { index, argument ->
                            matcher.addArgumentValue(name = argument.name) { expectedType ->
                                with(scope) { argument.compileValueOrNull(expectedType, onCoerced = { coercions++ }) }
                            }
                        }

                        if (!matcher.match { name, type -> Variable.Local(name, scope.nextLocalId, mutable = false, type) })
                            return@forEach logln("Matching failed for $function")
                        logln("Matching succeeded for $function")
                        options.add(Option(function, matcher, coercions))
                    }

                //TODO: Look up $call on locals for contexts
                //TODO: Look up regular $call on locals
                //TODO: Look up $props and their $call
            }

            when (lhs) {
                is Expression.NameReference -> resolveSimpleCalls(lhs.name, arguments)
                is Expression.DotReference -> {
                    //TODO: Look up functions with this receiver
                    val receiver = Expression.Call.Argument(lhs.lhs.location, name = "\$this", value = lhs.lhs)
                    resolveSimpleCalls(lhs.name, listOf(receiver) + arguments)

                    //TODO: Look up the field and try $call
                    //TODO: Look up $props and try $call
                }
                else -> location.error("idk how to call this sry man")
            }

            when {
                allFunctions.isEmpty() -> location.error("No such callable: $lhs")
                options.isEmpty() -> location.error(buildString {
                    appendLine("None of the following functions' signature matches the given arguments:")
                    allFunctions.forEach { appendLine("\t- $it") }
                })
                options.size == 1 -> return options.single().generate()
                else -> {
                    //TODO: Review that this actually does what it's supposed to do
                    options.filter { it.match.actualReturnType == expectedType }.let { filtered ->
                        if (filtered.isEmpty()) return@let
                        options.clear()
                        options.addAll(filtered)
                    }

                    val minCoercions = options.minOf { it.coercions }
                    logln(buildString {
                        appendLine("Matched functions:")
                        options.forEach { option ->
                            appendLine("\t- ${option.function} defined at ${option.function.location}")
                            /*option.arguments.forEach { argument ->
                                appendLine("\t\t${argument.argument.name} = ${argument.value}")
                            }*/
                        }
                    })
                    options.singleOrNull { it.coercions == minCoercions }?.generate() ?: location.error(buildString {
                        appendLine("Ambiguous call, all the following functions matched:")
                        options.forEach { option ->
                            appendLine("\t- `${option.function.name}` defined at ${option.function.location}")
                        }
                    })
                }
            }
        }

        is Expression.DotReference -> {
            val compiledLhs = lhs.compileValueExpression(expectedType = null)

            val field = compiledLhs.returnType.resolveFieldOrNull(name)
            if (field != null) Ir.Operation.FieldRead(compiledLhs, field)
            else Expression.Call(
                location,
                lhs = Expression.NameReference(location, "\$prop\$$name\$get"),
                explicitGenerics = emptyList(),
                arguments = listOf(Expression.Call.Argument(lhs.location, name = "\$this", lhs, compiledLhs))
            ).compileValueExpression(expectedType)
        }

        is Expression.NameReference -> {
            logln("Trying to resolve variable $name, current scope is:")
            dump()
            val variable = resolveVariableOrNull(name)
            when (variable) {
                null -> Expression.Call(
                    location,
                    lhs = Expression.NameReference(location, "\$prop\$$name\$get"),
                    explicitGenerics = emptyList(),
                    arguments = listOf(),
                ).compileValueExpression(expectedType)
                is Variable.LocalLike -> {
                    if (variable.mutable) Ir.Operation.LocalVarRead(variable)
                    else Ir.Operation.LocalValRead(variable)
                }
                is Variable.Global -> {
                    //TODO: Mutable?
                    Ir.Operation.GlobalValRead(variable)
                }
            }
        }

        is Expression.Loop -> {
            val scope = subscope("loop")
            val breakLabel = scope.createLabel(Label.Type.BREAK, expectedType = expectedType)
            val body = with(scope) { body.map { it.compileValueExpression(expectedType = null) } }
            scope.popLabel(breakLabel)

            Ir.Operation.Loop(breakLabel, body, breakLabel.resolveActualTypeOrNull() ?: ActualType.Nothing)
        }

        is Expression.When -> {
            val scope = subscope("when")
            val branches = branches.mapIndexed { index, branch ->
                val conditionScope = scope.subscope("when condition #$index")
                val condition = with(conditionScope) { branch.condition?.compileValueExpression(ActualType.Bool) }
                val bodyScope = conditionScope.subscope("when body #$index")
                if (condition!= null) retypeBranches(condition, positiveBranch = bodyScope, negativeBranch = scope)
                val body = with(bodyScope) { branch.body.map { it.compileValueExpression(expectedType = null) } }
                Ir.Operation.When.Branch(condition, body)
            }
            Ir.Operation.When(branches)
        }

        is Expression.Return -> {
            val returnLabel = resolveLabel(Label.Type.RETURN, location = location)
            if (returnLabel.expectedType != null && returnLabel.expectedType != ActualType.Void && value == null) location.error("Expected a return value")
            val value = value?.compileValueExpression(returnLabel.expectedType)
            value?.returnType?.let(returnLabel.actualTypes::add)
            Ir.Operation.Return(returnLabel, value)
        }

        is Expression.Break -> {
            val breakLabel = resolveLabel(Label.Type.BREAK, location = location)
            if (breakLabel.expectedType != null && breakLabel.expectedType != ActualType.Void && value == null) location.error("Expected a value")
            val value = value?.compileValueExpression(breakLabel.expectedType)
            value?.returnType?.let(breakLabel.actualTypes::add)
            Ir.Operation.Break(breakLabel, value)
        }

        is Expression.Assignment -> {
            when (lhs) {
                is Expression.NameReference -> {
                    val variable = resolveVariable(lhs.name, lhs.location)
                    when (variable) {
                        is Variable.LocalLike -> {
                            if (!variable.mutable) location.error("Cannot assign to `${lhs.name}` because it's not mutable")
                            Ir.Operation.LocalVarWrite(variable, rhs.compileValueExpression(variable.type))
                        }
                        is Variable.Global -> TODO("Compile global var write")
                    }
                }
                is Expression.DotReference -> {
                    val compiledLhs = lhs.lhs.compileValueExpression(expectedType = null)

                    val field = compiledLhs.returnType.resolveFieldOrNull(lhs.name)
                    if (field != null) Ir.Operation.FieldWrite(compiledLhs, field, rhs.compileValueExpression(expectedType = field.actualType))
                    else location.error("idk how to assign to this shi")
                }
                else -> location.error("idk how to assign to this shi")
            }
        }

        is Expression.StringLiteral -> {
            val type = stringLiteralType ?: location.error("String literals are not supported by the current configuration")
            Ir.Operation.StringLiteral(value, type)
        }

        is Expression.BooleanLiteral -> Ir.Operation.Bool(value)

        is Expression.NullLiteral -> Ir.Operation.NullLiteral()

        is Expression.NullCheck -> {
            val value = value.compileValueExpression(expectedType = null)
            Ir.Operation.NullCheck(value)
        }

        else -> location.error("Unexpected expression: $this")
    }

    logln("Compiled into $result")

    return when {
        expectedType == null -> result
        expectedType accepts result.returnType -> result
        else -> result.coerceInto(expectedType).also { onCoerced() } ?: run {
            if (allowNulls) null
            else location.error("Incompatible types, expected ${expectedType}, got ${result.returnType}")
        }
    }
}

context(Scope) fun Expression.compileValueExpression(expectedType: ActualType?, precompiledResult: Ir.Operation? = null): Ir.Operation =
    compileValueExpressionOrNull(expectedType, precompiledResult, allowNulls = false)!!

fun retypeBranches(condition: Ir.Operation, positiveBranch: Scope, negativeBranch: Scope) {
    when (condition) {
        is Ir.Operation.NullCheck -> {
            when (condition.value) {
                is Ir.Operation.LocalValRead -> {
                    val newLocal = condition.value.local.run { Variable.Local(
                        name, id,
                        mutable = false,
                        type = type.nullable(false),
                    ) }
                    positiveBranch.define(newLocal.name, newLocal)
                }
                else -> {/*pass*/}
            }
        }
        else -> {/*pass*/}
    }
}
