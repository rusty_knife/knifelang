package knife


interface Expression {
    val location: Location

    data class Type(
        override val location: Location,
        val name: String,
        val tags: List<Tag>,
        val genericTypes: List<GenericType>,
        val fields: List<Variable>,
        val functions: List<Function>,
        val properties: List<Property>,
    ) : Expression

    data class GenericType(
        override val location: Location,
        val name: String,
        val bounds: TypeReference?,
    ) : Expression

    data class TypeReference(
        override val location: Location,
        val name: String,
        val parameters: List<Parameter>,
        val nullable: Boolean,
    ) : Expression {
        data class Parameter(
            override val location: Location,
            val name: String?,
            val value: TypeReference,
        ) : Expression
    }

    data class Variable(
        override val location: Location,
        val name: String,
        val tags: List<Tag>,
        val type: TypeReference?,
        val initializer: Expression?,
        val mutable: Boolean,
    ) : Expression

    data class Tag(
        override val location: Location,
        val name: String,
        val arguments: List<Argument>
    ) : Expression {
        data class Argument(
            override val location: Location,
            val name: String?,
            val value: String,
        ) : Expression
    }

    data class Property(
        override val location: Location,
        val name: String,
        val tags: List<Tag>,
        val receiverType: TypeReference?,
        val type: TypeReference?,
        val accessors: List<Function>,
        val mutable: Boolean,
    ) : Expression

    data class NumericLiteral(
        override val location: Location,
        val value: String,
    ) : Expression

    data class StringLiteral(
        override val location: Location,
        val value: String,
        val quote: String,
    ) : Expression

    data class BooleanLiteral(
        override val location: Location,
        val value: Boolean,
    ) : Expression

    data class NullLiteral(override val location: Location) : Expression

    data class Function(
        override val location: Location,
        val name: String,
        val tags: List<Tag>,
        val genericTypes: List<GenericType>,
        val receiverType: TypeReference?,
        val arguments: List<Argument>,
        val returnType: TypeReference?,
        val body: List<Expression>?,
    ) : Expression {
        data class Argument(
            override val location: Location,
            val name: String,
            val type: TypeReference?,
            val default: Expression?,
            val mutable: Boolean,
        ) : Expression
    }

    data class NameReference(
        override val location: Location,
        val name: String,
    ) : Expression

    data class Call(
        override val location: Location,
        val lhs: Expression,
        val explicitGenerics: List<GenericArgument>,
        val arguments: List<Argument>,
    ) : Expression {
        data class GenericArgument(
            override val location: Location,
            val name: String?,
            val type: TypeReference,
        ) : Expression

        data class Argument(
            override val location: Location,
            val name: String?,
            val value: Expression,
            val precompiledValue: Ir.Operation? = null,
        ) : Expression
    }

    data class Assignment(
        override val location: Location,
        val lhs: Expression,
        val rhs: Expression,
    ) : Expression

    data class DotReference(
        override val location: Location,
        val lhs: Expression,
        val name: String,
    ) : Expression

    data class Loop(
        override val location: Location,
        val body: List<Expression>,
    ) : Expression

    data class When(
        override val location: Location,
        val branches: List<Branch>,
    ) : Expression {
        data class Branch(
            override val location: Location,
            val condition: Expression?,
            val body: List<Expression>,
        ) : Expression
    }

    data class Return(
        override val location: Location,
        val value: Expression?,
    ) : Expression

    data class Break(
        override val location: Location,
        val value: Expression?,
    ) : Expression

    data class NullCheck(
        override val location: Location,
        val value: Expression,
    ) : Expression
}
